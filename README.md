# Ryan Engine

A Very simple Opengl engine that uses SDL. 

The plan is to implement a lot of basic common stuff so I can ramp up ideas faster.
I don't want to make some massive layer on top of everything that hides all the
complexity of raw GL. That isn't the point of this engine. I want to make 
it modular so you can alway pull things appart. I will not be making
an Input layer on top of SDL or any other kinds of unification. SDL and OpenGL 
have good documentation and building a layer on top that would expose 
me to a lot of duplication errors and documentation.

I want this to be open source so others can use it. I just don't know what
licence I want to use yet.

## Features

* A logging system
* Shader loading from file, compiling, and linkning.
* Vector Utilities
* Matrix Utilities
* Transform Utilties (TODO)
* Geometry Building Utilities (TODO)

Maybe some defered Rendering and shadows, I would like to do a cavity shader.

## Build

See [BUILD.md](doc/BUILD.md)
