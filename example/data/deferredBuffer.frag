#version 330 core
// === Deferred Buffer Shader Vertex === //
layout (location = 0) out vec3 gbPosition;
layout (location = 1) out vec3 gbNormal;
layout (location = 2) out vec3 gbColor;

in vec3 normal;
in vec3 fragPos;

void main()
{
   gbPosition = fragPos;
   gbColor = vec3(0.5, 0.0, 0.0);
   gbNormal = normalize(normal);
}

