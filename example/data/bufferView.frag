#version 330 core
// === Buffer View Shader Fragment === //

out vec4 FragColor;

in vec2 textureCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gColor;


void make_kernel(inout vec4 n[9], sampler2D tex, vec2 coord)
{
   // Delta Pixel
   vec2 dp = vec2(1 / 620.0, 1 / 387.0);
   n[0] = texture(tex, coord + vec2(-dp.x,  dp.y));
   n[1] = texture(tex, coord + vec2( 0.0,   dp.y));
   n[2] = texture(tex, coord + vec2( dp.x,  dp.y));
   n[3] = texture(tex, coord + vec2(-dp.x,  0.0));
   n[4] = texture(tex, coord);
   n[5] = texture(tex, coord + vec2( dp.x,  0.0));
   n[6] = texture(tex, coord + vec2(-dp.x, -dp.y));
   n[7] = texture(tex, coord + vec2( 0.0,  -dp.y));
   n[8] = texture(tex, coord + vec2( dp.x, -dp.y));
}

vec4 convolute(in vec4 a[9], in float b[9])
{
   vec4 result = vec4(0, 0, 0, 0);
   for(int i = 0; i < 9; i++)
   {
      result += a[i] * b[i];
   }
   return result;
}

void main()
{
   vec2 imgTextureCoords = textureCoords * 2;
   if(imgTextureCoords.s <= 1 && imgTextureCoords.t > 1)
   {
      FragColor = vec4((texture(gNormal, vec2(imgTextureCoords.s, imgTextureCoords.t - 1)).rgb + 1) / 2, 1);
   }
   if(imgTextureCoords.s > 1 && imgTextureCoords.t > 1)
   {
      //FragColor = vec4(((texture(gPosition, vec2(imgTextureCoords.s - 1, imgTextureCoords.t - 1)).rgb + 1) / 2), 1);
      vec3 pos = texture(gPosition, vec2(imgTextureCoords.s - 1, imgTextureCoords.t - 1)).rgb;
      if(abs(pos.z) < 0.01)
      {
         FragColor = vec4(0, 0, 0, 1);
      }
      else
      {
         FragColor = mix(vec4(1,0,0,1), vec4(0, 0, 1, 1), (-pos.z - 2) / 2);
      }
   }
   if(imgTextureCoords.s <= 1 && imgTextureCoords.t <= 1)
   {
      FragColor = vec4(texture(gColor, vec2(imgTextureCoords.s, imgTextureCoords.t)));
   }
   if(imgTextureCoords.s > 1 && imgTextureCoords.t <= 1)
   {
      /*
      vec4 n[9];
      float ky[9] = float[](  1.0,  2.0,  1.0,
                              0.0,  0.0,  0.0,
                             -1.0, -2.0, -1.0 );
      float kx[9] = float[](  1.0,  0.0, -1.0,
                              2.0,  0.0, -2.0,
                              1.0,  0.0, -1.0 );
      vec2 localTextCoords = vec2(imgTextureCoords.s - 1, imgTextureCoords.t);
      vec4 color = texture(gColor, localTextCoords);
      make_kernel(n, gNormal, localTextCoords);
      
      FragColor = color;
      vec3 edgeY = convolute(n, ky).rgb;
      vec3 edgeX = convolute(n, kx).rgb;
      vec3 average = (edgeX + edgeY); 
      //FragColor = (average + 1) / 2;
      //FragColor = edgeX;
      //FragColor = vec4(normalize(edgeX), 1);
      if(length(edgeX) < 0.1)
      {
         FragColor = vec4(0, 0, 0, 1);
      }
      else
      {
         FragColor = vec4(abs(normalize(edgeX)), 1);
      }
      */
      vec4 n[9];
      float ky[9] = float[](  1.0,  2.0,  1.0,
                              0.0, -6.0,  0.0,
                              1.0,  2.0,  1.0 );
      float kx[9] = float[](  1.0,  0.0,  1.0,
                              2.0, -6.0,  2.0,
                              1.0,  0.0,  1.0 );
      vec2 localTextCoords = vec2(imgTextureCoords.s - 1, imgTextureCoords.t);
      vec4 color = texture(gColor, localTextCoords);
      make_kernel(n, gPosition, localTextCoords);
      
      FragColor = color;
      vec3 edgeY = convolute(n, ky).rgb;
      vec3 edgeX = convolute(n, kx).rgb;
      vec3 average = (edgeX + edgeY); 
      //FragColor = (average + 1) / 2;
      //FragColor = edgeX;
      //FragColor = vec4(normalize(edgeX), 1);
      if(abs(edgeX.z) < 0.1)
      {
         FragColor = vec4(0, 0, 0, 1);
      }
      else
      {
         float zNorm = (-edgeX.z - 1) / 99; 
         //FragColor = mix(vec4(vec3((edgeX.z + 4) / 8), 1), color, 0.5);
         //FragColor = vec4(vec3((edgeX.z + 1) / 2), 1);
         //FragColor = vec4(vec3(abs(edgeX.z) / 8), 1);
         FragColor = vec4(vec3(zNorm) * 2, 1);
      }
   }

}

