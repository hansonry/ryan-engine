#version 330 core
// === Buffer View Shader Vertex === //
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm; // Intentionaly Not used
layout (location = 2) in vec2 aUV;

uniform mat4 matrixPCM;

out vec2 textureCoords;

void main()
{
   gl_Position =  matrixPCM * vec4(aPos, 1.0);
   textureCoords = aUV;
}

