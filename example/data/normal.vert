#version 330 core
// === Normal Shader Vertex  === //
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;

uniform mat4 matrixPCM;
uniform mat4 matrixM;

out vec3 normal;

void main()
{
   gl_Position =  matrixPCM * vec4(aPos, 1.0);
   normal = (matrixM * vec4(aNorm, 1.0)).xyz;
   //gl_Position = vec4(aPos, 1.0);
}

