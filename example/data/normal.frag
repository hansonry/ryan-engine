#version 330 core
// === Normal Shader Fragment === //
out vec4 FragColor;

in vec3 normal;

void main()
{
   FragColor = vec4(0.5, 0.0, 0.0, 1.0);
   vec3 normalizedNormal = normal; //normalize(normal);
   FragColor = vec4((normalizedNormal + 1) / 2 , 1.0);
}

