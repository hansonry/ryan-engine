#include <stdio.h>
#include <RyanEngine.h>

static void renderFunction(void * object);

static void application(struct reStateTreeNode * node, 
                        struct reStateTreeNodeData * data)
{
   (void)node;
   data->cbRender = renderFunction;
}

int main(int argc, char * args[])
{
   return reWindow_runDefaults(application);
}

static void renderFunction(void * object)
{
   (void)object;
   glClear(GL_COLOR_BUFFER_BIT);
}

