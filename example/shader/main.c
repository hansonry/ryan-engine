#include "stdio.h"
#include <RyanEngine.h>

#include <RyanGLRenderer.h>

static void renderFunction(void * object);
static void windowCreatedFunction(SDL_Window * window, 
                                  int viewPortWidth, 
                                  int viewPortHeight);
static void loadFunction(void * object);
static void unloadFunction(void * object);
static void updateFunction(void * object, float seconds);

static void application(struct reStateTreeNode * node, 
                        struct reStateTreeNodeData * data)
{
   (void)node;
   data->cbRender = renderFunction;
   data->cbLoad = loadFunction;
   data->cbUnload = unloadFunction;
   data->cbUpdate = updateFunction;
}


static struct rrprogram * program;
static struct rruniform * uniformMatrix;
static struct rrgeometry * geometry, * square;
static struct ryanrenderer * rr;
static struct ryanscope rs;



// TODO: Remove Matrix testing junk as it will dirty this example
static float angle;
static float per[REMAT4F_SIZE];

int main(int argc, char * args[])
{
   reWindow_setCreatedCallback(windowCreatedFunction);
   return reWindow_runDefaults(application);
}

static void windowCreatedFunction(SDL_Window * window, 
                                  int viewPortWidth, 
                                  int viewPortHeight)
{
   // setup perspective
   reMat4f_orthoAspect(per, 2, viewPortWidth / (float)viewPortHeight, 1, -1);
}


static void loadFunction(void * object)
{
   struct ryangeometrybuilder rgb;
   struct rrattribute * attribute;
   (void)object;

   rsInit(&rs);

   rr = rrCreateGLRenderer();
   rsAddObj(&rs, rr);

   rrSetClearColor(rr, &rcColorBlueBackground);

   angle = 0;

   program = rrCreateProgramFromFiles(rr, "example/shader/red.vert", 
                                          NULL,
                                          "example/shader/red.frag");
   rsAddObj(&rs, program);

   uniformMatrix = rrpCreateUniformFromName(program, "matrix");
   rsAddObj(&rs, uniformMatrix);
   
   attribute = rrpCreateAttributeFromIndex(program, 0);
   
   rgbInit(&rgb, "vertex[3]");
   
   rgbSetProgramAttribute(&rgb, "vertex", attribute);
   
   rgbSetVertexValue(&rgb, "vertex",  0.0,  0.5, 0.0);
   rgbAddVertexAndIndex(&rgb);
   rgbSetVertexValue(&rgb, "vertex", -0.5, -0.5, 0.0);
   rgbAddVertexAndIndex(&rgb);
   rgbSetVertexValue(&rgb, "vertex",  0.5, -0.5, 0.0);
   rgbAddVertexAndIndex(&rgb);
   geometry = rgbCreateRenderableGeometry(&rgb, rr, eRRDT_Float32,  
                                                    eRRDT_UInt32,  
                                                    eRRDM_Triangles);
   rsAddObj(&rs, geometry);
   rsAddObj(&rs, rrgGetIndexBuffer(geometry));
   rsAddObj(&rs, rrgGetVertexBuffer(geometry));
   
   rgbClearData(&rgb);

   rgbSetVertexValue(&rgb, "vertex", -0.1, -0.1, 0.0);
   rgbAddVertex(&rgb);
   rgbSetVertexValue(&rgb, "vertex",  0.1, -0.1, 0.0);
   rgbAddVertex(&rgb);
   rgbSetVertexValue(&rgb, "vertex",  0.1,  0.1, 0.0);
   rgbAddVertex(&rgb);
   rgbSetVertexValue(&rgb, "vertex", -0.1,  0.1, 0.0);
   rgbAddVertex(&rgb);
   rgbAddIndex(&rgb, 0);
   rgbAddIndex(&rgb, 3);
   rgbAddIndex(&rgb, 1);
   rgbAddIndex(&rgb, 3);
   rgbAddIndex(&rgb, 2);
   rgbAddIndex(&rgb, 1);
   
   square = rgbCreateRenderableGeometry(&rgb, rr, eRRDT_Float32,  
                                                  eRRDT_UInt32,  
                                                  eRRDM_Triangles);
   rsAddObj(&rs, square);
   rsAddObj(&rs, rrgGetIndexBuffer(square));
   rsAddObj(&rs, rrgGetVertexBuffer(square));
   

   rgbCleanup(&rgb);

   rraDestroy(attribute);

}

static void unloadFunction(void * object)
{
   (void)object;
   rsCleanup(&rs);
   
}

static void updateFunction(void * object, float seconds)
{
   (void)object;
   angle += 3.14 / 4 * seconds;
}

static void renderFunction(void * object)
{
   float mat[REMAT4F_SIZE];
   float model[REMAT4F_SIZE];
   (void)object;
   reTForm_setIdentity(model);
   reTForm_translate(model, 1, 0, 0);
   reTForm_rotateZ(model, angle);
   reMat4f_multiplyPCM(mat, NULL, per, NULL, model);


   rrClearColorAndDepth(rr);
   rrpUse(program);
   rruSetMatrix4f(uniformMatrix, mat, 1);
   rrgDraw(geometry);
   
   reTForm_setIdentity(model);
   rruSetMatrix4f(uniformMatrix, model, 1);
   rrgDraw(square);
}

