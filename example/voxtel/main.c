#include "stdio.h"
#include <RyanEngine.h>


static GLuint deferredBufferProgram;
static GLint uniformDBMatrixPCM;
static GLint uniformDBMatrixM;

static GLuint bufferViewProgram;
static GLint uniformBVMatrixPCM;
static GLint uniformBVTexturePosition;
static GLint uniformBVTextureNormal;
static GLint uniformBVTextureColor;

static GLuint gBuffer;
static GLuint gbPosition, gbNormal, gbColor, gdDepth;
static struct reShape cube, rectangle;

// TODO: Remove Matrix testing junk as it will dirty this example
static float angle;


static struct reCameraOrbit cam;
static float per[REMAT4F_SIZE];

static void renderFunction(void * object);
static void createWindowFunction(SDL_Window * window, 
                                 int viewPortWidth, 
                                 int viewPortHeight);
static void loadFunction(void * object);
static void unloadFunction(void * object);
static void updateFunction(void * object, float seconds);
static void eventFunction(void * object, SDL_Event * event);

static void application(struct reStateTreeNode * node, 
                        struct reStateTreeNodeData * data)
{
   (void)node;
   data->cbRender = renderFunction;
   data->cbLoad = loadFunction;
   data->cbUnload = unloadFunction;
   data->cbUpdate = updateFunction;
   data->cbEvent = eventFunction;
   reCameraOrbit_addNode(&cam, node);
}

int main(int argc, char * args[])
{
   reWindow_setCreatedCallback(createWindowFunction);
   return reWindow_runDefaults(application);
}

static void framebufferCreate(int width, int height)
{
   GLuint drawBufferTargets[3];
   
   glGenFramebuffers(1, &gBuffer);
   glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
   
   /// Position
   glGenTextures(1, &gbPosition);
   glBindTexture(GL_TEXTURE_2D, gbPosition);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gbPosition, 0);
   drawBufferTargets[0] = GL_COLOR_ATTACHMENT0;

   /// normal
   glGenTextures(1, &gbNormal);
   glBindTexture(GL_TEXTURE_2D, gbNormal);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gbNormal, 0);
   drawBufferTargets[1] = GL_COLOR_ATTACHMENT1;
   
   /// color
   glGenTextures(1, &gbColor);
   glBindTexture(GL_TEXTURE_2D, gbColor);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gbColor, 0);
   drawBufferTargets[2] = GL_COLOR_ATTACHMENT2;
   
   /// Depth
   glGenTextures(1, &gdDepth);
   glBindTexture(GL_TEXTURE_2D, gdDepth);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, gdDepth, 0);
   
   /// Setup Target Buffers
   glDrawBuffers(3, drawBufferTargets);
}

static void framebufferDestroy(void)
{
   // Frame Buffer
   glDeleteFramebuffers(1, &gBuffer);
   glDeleteTextures(1, &gbPosition);
   glDeleteTextures(1, &gbNormal);
   glDeleteTextures(1, &gbColor);
   glDeleteTextures(1, &gdDepth);
}


static void loadFunction(void * object)
{
   int width, height;
   GLenum errorCode;
   (void)object;
   reWindow_getSize(&width, &height);
   
   // Load Shaders and get Uniform Locations 
   deferredBufferProgram = reShader_loadCompileAndLink("example/data/deferredBuffer.vert", 
                                                       NULL,
                                                       "example/data/deferredBuffer.frag");


   uniformDBMatrixPCM = glGetUniformLocation(deferredBufferProgram, "matrixPCM");
   uniformDBMatrixM   = glGetUniformLocation(deferredBufferProgram, "matrixM");
   
   bufferViewProgram = reShader_loadCompileAndLink("example/data/bufferView.vert", 
                                                   NULL,
                                                   "example/data/bufferView.frag");
                                                   
   uniformBVMatrixPCM = glGetUniformLocation(bufferViewProgram, "matrixPCM");
   uniformBVTexturePosition = glGetUniformLocation(bufferViewProgram, "gPosition");
   uniformBVTextureNormal = glGetUniformLocation(bufferViewProgram, "gNormal");
   uniformBVTextureColor = glGetUniformLocation(bufferViewProgram, "gColor");


   // Setup GL State
   glEnableClientState(GL_VERTEX_ARRAY);

   /// Back Face Culling
   glEnable(GL_CULL_FACE);
   glCullFace(GL_BACK);
   glFrontFace(GL_CCW);

   /// Depth Test
   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);
   glDepthMask(GL_TRUE);

   // Deferred Shading Setup
   framebufferCreate(width, height);
 
   
   // Create Geometry
   reShape_cube(&cube);
   reShape_rectangle(&rectangle, -1, -1, 1, 1);

   // Initalize State
   
   /// Initalize Camera
   reCameraOrbit_setAll(&cam, 0, 0, 0, 2, 0, 0);

   /// Other State
   angle = 0;
   
}

static void createWindowFunction(SDL_Window * window, 
                                 int viewPortWidth, 
                                 int viewPortHeight)
{
   reMat4f_perspective(per, 
                       viewPortWidth / (float)viewPortHeight, 
                       60 * 3.14 / 180, 
                       1, 10);

   SDL_SetWindowResizable(window, SDL_TRUE);
}

static void unloadFunction(void * object)
{
   (void)object;
   
   glDeleteProgram(deferredBufferProgram);
   glDeleteProgram(bufferViewProgram);
   reShape_teardown(&rectangle);
   reShape_teardown(&cube);

   framebufferDestroy();
}

static void updateFunction(void * object, float seconds)
{
   //angle += 3.14 / 4 * seconds;
}

static void eventFunction(void * object, SDL_Event * event)
{  
   (void)object;
   if(event->type == SDL_WINDOWEVENT)
   {
      if(event->window.event == SDL_WINDOWEVENT_RESIZED)
      {
         int width  = event->window.data1;
         int height = event->window.data2;
         glViewport(0, 0, width, height);
         reMat4f_perspective(per, 
                             width/ (float)height, 
                             60 * 3.14 / 180, 
                             1, 100);
         framebufferDestroy();
         framebufferCreate(width, height);
      }
   }
}

static void renderFunction(void * object)
{
   float matPCM[REMAT4F_SIZE];
   float matC[REMAT4F_SIZE]; 
   float matM[REMAT4F_SIZE];
   float matIT[REMAT4F_SIZE];
   (void)object;
   reTForm_setIdentity(matM);
   reTForm_translate(matM, -0.5f, -0.5f, -0.5f);
   reTForm_rotateY(matM, angle);
   reCameraOrbit_computeMatrix(&cam, matC);
   reMat4f_multiplyPCM(matPCM, matIT, per, matC, matM);


   //glBindFramebuffer(GL_FRAMEBUFFER, 0);
   glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
   glClearColor(0, 0, 0, 1);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glUseProgram(deferredBufferProgram);
   reMat4f_setUniform(uniformDBMatrixPCM, 1, matPCM);
   reMat4f_setUniform(uniformDBMatrixM,   1, matIT);
   reShape_draw(&cube);

   reTForm_translate(matM, 0.5f, 0.5f, 0.5f);
   reMat4f_multiplyPCM(matPCM, matIT, per, matC, matM);
   
   reMat4f_setUniform(uniformDBMatrixPCM, 1, matPCM);
   reMat4f_setUniform(uniformDBMatrixM,   1, matIT);
   reShape_draw(&cube);
   glUseProgram(0);
   
   
   reMat4f_identity(matPCM);
   glBindFramebuffer(GL_FRAMEBUFFER, 0);
   glClearColor(0.0f, 0.0f, 0.15f, 1.0f);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glUseProgram(bufferViewProgram);
   reMat4f_setUniform(uniformBVMatrixPCM, 1, matPCM);
   glActiveTexture(GL_TEXTURE0);
   glBindTexture(GL_TEXTURE_2D, gbPosition);
   glUniform1i(uniformBVTexturePosition, 0);
   glActiveTexture(GL_TEXTURE1);
   glBindTexture(GL_TEXTURE_2D, gbNormal);
   glUniform1i(uniformBVTextureNormal, 1);
   glActiveTexture(GL_TEXTURE2);
   glBindTexture(GL_TEXTURE_2D, gbColor);
   glUniform1i(uniformBVTextureColor, 2);
   reShape_draw(&rectangle);
   glUseProgram(0);
}

