#ifndef __REMAT4F_H__
#define __REMAT4F_H__
#include <stddef.h>
#include <math.h>
#include <reVec3f.h>


#define REMAT4F_SIZE   16


#define REMAT4F_SET(mat, c11, c12, c13, c14, c21, c22, c23, c24, c31, c32, c33, c34, c41, c42, c43, c44) \
(mat)[ 0] = c11; (mat)[ 1] = c21; (mat)[ 2] = c31; (mat)[ 3] = c41; \
(mat)[ 4] = c12; (mat)[ 5] = c22; (mat)[ 6] = c32; (mat)[ 7] = c42; \
(mat)[ 8] = c13; (mat)[ 9] = c23; (mat)[10] = c33; (mat)[11] = c43; \
(mat)[12] = c14; (mat)[13] = c24; (mat)[14] = c34; (mat)[15] = c44



static inline float * reMat4f_identity(float * mat)
{
   REMAT4F_SET(mat,
               1, 0, 0, 0,
               0, 1, 0, 0,
               0, 0, 1, 0,
               0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_scale(float * mat, float x, float y, float z)
{
   REMAT4F_SET(mat,
               x, 0, 0, 0,
               0, y, 0, 0,
               0, 0, z, 0,
               0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_scaleAll(float * mat, float k)
{
   REMAT4F_SET(mat,
               k, 0, 0, 0,
               0, k, 0, 0,
               0, 0, k, 0,
               0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_translate(float * mat, float x, float y, float z)
{
   REMAT4F_SET(mat,
               1, 0, 0, x,
               0, 1, 0, y,
               0, 0, 1, z,
               0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_translateVec3f(float * mat, const struct reVec3f * vec)
{

   REMAT4F_SET(mat,
               1, 0, 0, vec->x,
               0, 1, 0, vec->y,
               0, 0, 1, vec->z,
               0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_rotateX(float * mat, float radians)
{
   float s = sin(radians);
   float c = cos(radians);
   REMAT4F_SET(mat,
               1, 0,  0, 0,
               0, c, -s, 0,
               0, s,  c, 0,
               0, 0,  0, 1);
   return mat;
}

static inline float * reMat4f_rotateY(float * mat, float radians)
{
   float s = sin(radians);
   float c = cos(radians);
   REMAT4F_SET(mat,
                c, 0, s, 0,
                0, 1, 0, 0,
               -s, 0, c, 0,
                0, 0, 0, 1);
   return mat;
}

static inline float * reMat4f_rotateZ(float * mat, float radians)
{
   float s = sin(radians);
   float c = cos(radians);
   REMAT4F_SET(mat,
               c, -s, 0, 0,
               s,  c, 0, 0,
               0,  0, 1, 0,
               0,  0, 0, 1);
   return mat;
}

/**
 * Rotate some angle around the normalized vector
 *
 * The passed in matrix will be overwritten with a rotation matrix represented
 * by an angle around a nomalized vector.
 *
 * @param mat The Matrix
 * @oaram radians The angle in radians to rotate
 * @param nx The X component of the normalized vector to rotate around
 * @param ny The Y component of the normalized vector to rotate around
 * @param nz The Z component of the normalized vector to rotate around
 * @return The matrix that was passed in.
 */
static inline float * reMat4f_rotate(float * mat, float radians, float nx, float ny, float nz)
{
   float vs = sin(radians);
   float vc = cos(radians);
   float v1mc = 1 - vc;
   float vxy = nx * ny;
   float vxz = nx * nz;
   float vyz = ny * nz;
   REMAT4F_SET(mat,
               vc + nx * nx * v1mc,   vxy * v1mc - nz * vs,  vxz * v1mc + ny * vs, 0,
               vxy * v1mc + nz * vs,  vc + ny * ny * v1mc,   vyz * v1mc - nx * vs, 0,
               vxz * v1mc - ny * vs,  vyz * v1mc + nx * vs,  vc + nz * nz * v1mc,  0,
               0,                     0,                     0,                    1);
   return mat;
}

static inline float * reMat4f_rotateVec(float * mat, float radians, const struct reVec3f * nVec)
{
   float vs = sin(radians);
   float vc = cos(radians);
   float v1mc = 1 - vc;
   float vxy = nVec->x * nVec->y;
   float vxz = nVec->x * nVec->z;
   float vyz = nVec->y * nVec->z;
   REMAT4F_SET(mat,
               vc + nVec->x * nVec->x * v1mc,   vxy * v1mc - nVec->z * vs,       vxz * v1mc + nVec->y * vs,      0,
               vxy * v1mc + nVec->z * vs,       vc + nVec->y * nVec->y * v1mc,   vyz * v1mc - nVec->x * vs,      0,
               vxz * v1mc - nVec->y * vs,       vyz * v1mc + nVec->x * vs,       vc + nVec->z * nVec->z * v1mc,  0,
               0,                               0,                               0,                              1);
   return mat;
}

static inline float * reMat4f_copy(float * dest, const float * src)
{
   dest[ 0] = src[ 0];
   dest[ 1] = src[ 1];
   dest[ 2] = src[ 2];
   dest[ 3] = src[ 3];
   dest[ 4] = src[ 4];
   dest[ 5] = src[ 5];
   dest[ 6] = src[ 6];
   dest[ 7] = src[ 7];
   dest[ 8] = src[ 8];
   dest[ 9] = src[ 9];
   dest[10] = src[10];
   dest[11] = src[11];
   dest[12] = src[12];
   dest[13] = src[13];
   dest[14] = src[14];
   dest[15] = src[15];
   return dest;
}

static inline float * reMat4f_mulitply(float * mat, const float * l, const float * r)
{
   float temp[REMAT4F_SIZE];
   temp[ 0] = l[ 0] * r[ 0] + l[ 4] * r[ 1] + l[ 8] * r[ 2] + l[12] * r[ 3]; 
   temp[ 1] = l[ 1] * r[ 0] + l[ 5] * r[ 1] + l[ 9] * r[ 2] + l[13] * r[ 3]; 
   temp[ 2] = l[ 2] * r[ 0] + l[ 6] * r[ 1] + l[10] * r[ 2] + l[14] * r[ 3]; 
   temp[ 3] = l[ 3] * r[ 0] + l[ 7] * r[ 1] + l[11] * r[ 2] + l[15] * r[ 3]; 

   temp[ 4] = l[ 0] * r[ 4] + l[ 4] * r[ 5] + l[ 8] * r[ 6] + l[12] * r[ 7]; 
   temp[ 5] = l[ 1] * r[ 4] + l[ 5] * r[ 5] + l[ 9] * r[ 6] + l[13] * r[ 7]; 
   temp[ 6] = l[ 2] * r[ 4] + l[ 6] * r[ 5] + l[10] * r[ 6] + l[14] * r[ 7]; 
   temp[ 7] = l[ 3] * r[ 4] + l[ 7] * r[ 5] + l[11] * r[ 6] + l[15] * r[ 7]; 

   temp[ 8] = l[ 0] * r[ 8] + l[ 4] * r[ 9] + l[ 8] * r[10] + l[12] * r[11]; 
   temp[ 9] = l[ 1] * r[ 8] + l[ 5] * r[ 9] + l[ 9] * r[10] + l[13] * r[11]; 
   temp[10] = l[ 2] * r[ 8] + l[ 6] * r[ 9] + l[10] * r[10] + l[14] * r[11]; 
   temp[11] = l[ 3] * r[ 8] + l[ 7] * r[ 9] + l[11] * r[10] + l[15] * r[11]; 

   temp[12] = l[ 0] * r[12] + l[ 4] * r[13] + l[ 8] * r[14] + l[12] * r[15]; 
   temp[13] = l[ 1] * r[12] + l[ 5] * r[13] + l[ 9] * r[14] + l[13] * r[15]; 
   temp[14] = l[ 2] * r[12] + l[ 6] * r[13] + l[10] * r[14] + l[14] * r[15]; 
   temp[15] = l[ 3] * r[12] + l[ 7] * r[13] + l[11] * r[14] + l[15] * r[15]; 
   
   return reMat4f_copy(mat, temp);
}


static inline float * reMat4f_transpose(float * out, const float * in)
{
   float t[REMAT4F_SIZE];
   (void)reMat4f_copy(t, in);
   out[ 0] = t[ 0];
   out[ 1] = t[ 4];
   out[ 2] = t[ 8];
   out[ 3] = t[12];

   out[ 4] = t[ 1];
   out[ 5] = t[ 5];
   out[ 6] = t[ 9];
   out[ 7] = t[13];
   
   out[ 8] = t[ 2];
   out[ 9] = t[ 6];
   out[10] = t[10];
   out[11] = t[14];
   
   out[12] = t[ 3];
   out[13] = t[ 7];
   out[14] = t[11];
   out[15] = t[15];
   return out;
}

static inline struct reVec3f * reMat4f_multiplyVec(struct reVec3f * vec, const float * mat)
{
   struct reVec3f t;
   reVec3f_copy(&t, vec);
   vec->x = t.x * mat[ 0] + t.y * mat[ 4] + t.z * mat[ 8] + mat[12];
   vec->y = t.x * mat[ 1] + t.y * mat[ 5] + t.z * mat[ 9] + mat[13];
   vec->z = t.x * mat[ 2] + t.y * mat[ 6] + t.z * mat[10] + mat[14];
   return vec;
}

float * reMat4f_multiplyAll(float * dest, int count, ...);





static inline float * reMat4f_orthoAspect(float * mat, float height, float aspectRatio, float zNear, float zFar)
{
   float width = aspectRatio * height;
   REMAT4F_SET(mat,
               2 / width, 0,          0,                  0,
               0,         2 / height, 0,                  0,
               0,         0,          2 / (zFar - zNear), -(zFar + zNear) / (zFar - zNear),
               0,         0,          0,                  1);
   return mat;
}

static inline float * reMat4f_perspective(float * mat, float aspectRatio, float fovInRadians, float zNear, float zFar)
{
   float vTFOVo2 = tan(fovInRadians / 2);
   float vZDiff = zFar - zNear;
   REMAT4F_SET(mat,
               1 / (aspectRatio * vTFOVo2), 0,           0,                            0,
               0,                           1 / vTFOVo2, 0,                            0,
               0,                           0,           -(zFar + zNear) / vZDiff,    -1,
               0,                           0,           (2 * zFar * zNear) / vZDiff,  1);
   return mat;
}

/** 
 * This function invertes the specified matrix.
 *
 * The out and in parameters can point to the same memory. If the 
 * source matrix can't be inverted (the determinant is 0), the output 
 * matrix will be unchanged and this function will return 1.
 *
 * @param out  The desination matrix
 * @param in   The source matrix to be inverted.
 * @return     1 if the determinant is 0 and the source matrix can't 
 *             be inverted, otherwise 0 
 */
static inline int reMat4f_invert(float * out, const float * in)
{
   float temp[16];
   float det;
   int i;

   temp[0] = in[5]  * in[10] * in[15] - 
             in[5]  * in[11] * in[14] - 
             in[9]  * in[6]  * in[15] + 
             in[9]  * in[7]  * in[14] +
             in[13] * in[6]  * in[11] - 
             in[13] * in[7]  * in[10];

   temp[4] = -in[4]  * in[10] * in[15] + 
              in[4]  * in[11] * in[14] + 
              in[8]  * in[6]  * in[15] - 
              in[8]  * in[7]  * in[14] - 
              in[12] * in[6]  * in[11] + 
              in[12] * in[7]  * in[10];

   temp[8] = in[4]  * in[9] * in[15] - 
             in[4]  * in[11] * in[13] - 
             in[8]  * in[5] * in[15] + 
             in[8]  * in[7] * in[13] + 
             in[12] * in[5] * in[11] - 
             in[12] * in[7] * in[9];

   temp[12] = -in[4]  * in[9] * in[14] + 
               in[4]  * in[10] * in[13] +
               in[8]  * in[5] * in[14] - 
               in[8]  * in[6] * in[13] - 
               in[12] * in[5] * in[10] + 
               in[12] * in[6] * in[9];

   temp[1] = -in[1]  * in[10] * in[15] + 
              in[1]  * in[11] * in[14] + 
              in[9]  * in[2] * in[15] - 
              in[9]  * in[3] * in[14] - 
              in[13] * in[2] * in[11] + 
              in[13] * in[3] * in[10];

   temp[5] = in[0]  * in[10] * in[15] - 
             in[0]  * in[11] * in[14] - 
             in[8]  * in[2] * in[15] + 
             in[8]  * in[3] * in[14] + 
             in[12] * in[2] * in[11] - 
             in[12] * in[3] * in[10];

   temp[9] = -in[0]  * in[9] * in[15] + 
              in[0]  * in[11] * in[13] + 
              in[8]  * in[1] * in[15] - 
              in[8]  * in[3] * in[13] - 
              in[12] * in[1] * in[11] + 
              in[12] * in[3] * in[9];

   temp[13] = in[0]  * in[9] * in[14] - 
              in[0]  * in[10] * in[13] - 
              in[8]  * in[1] * in[14] + 
              in[8]  * in[2] * in[13] + 
              in[12] * in[1] * in[10] - 
              in[12] * in[2] * in[9];

   temp[2] = in[1]  * in[6] * in[15] - 
             in[1]  * in[7] * in[14] - 
             in[5]  * in[2] * in[15] + 
             in[5]  * in[3] * in[14] + 
             in[13] * in[2] * in[7] - 
             in[13] * in[3] * in[6];

   temp[6] = -in[0]  * in[6] * in[15] + 
              in[0]  * in[7] * in[14] + 
              in[4]  * in[2] * in[15] - 
              in[4]  * in[3] * in[14] - 
              in[12] * in[2] * in[7] + 
              in[12] * in[3] * in[6];

   temp[10] = in[0]  * in[5] * in[15] - 
              in[0]  * in[7] * in[13] - 
              in[4]  * in[1] * in[15] + 
              in[4]  * in[3] * in[13] + 
              in[12] * in[1] * in[7] - 
              in[12] * in[3] * in[5];

   temp[14] = -in[0]  * in[5] * in[14] + 
               in[0]  * in[6] * in[13] + 
               in[4]  * in[1] * in[14] - 
               in[4]  * in[2] * in[13] - 
               in[12] * in[1] * in[6] + 
               in[12] * in[2] * in[5];

   temp[3] = -in[1] * in[6] * in[11] + 
              in[1] * in[7] * in[10] + 
              in[5] * in[2] * in[11] - 
              in[5] * in[3] * in[10] - 
              in[9] * in[2] * in[7] + 
              in[9] * in[3] * in[6];

   temp[7] = in[0] * in[6] * in[11] - 
             in[0] * in[7] * in[10] - 
             in[4] * in[2] * in[11] + 
             in[4] * in[3] * in[10] + 
             in[8] * in[2] * in[7] - 
             in[8] * in[3] * in[6];

   temp[11] = -in[0] * in[5] * in[11] + 
               in[0] * in[7] * in[9] + 
               in[4] * in[1] * in[11] - 
               in[4] * in[3] * in[9] - 
               in[8] * in[1] * in[7] + 
               in[8] * in[3] * in[5];

   temp[15] = in[0] * in[5] * in[10] - 
              in[0] * in[6] * in[9] - 
              in[4] * in[1] * in[10] + 
              in[4] * in[2] * in[9] + 
              in[8] * in[1] * in[6] - 
              in[8] * in[2] * in[5];

   det = in[0] * temp[0] + in[1] * temp[4] + in[2] * temp[8] + in[3] * temp[12];

   if (det == 0)
       return 1;

   det = 1.0 / det;

   for (i = 0; i < 16; i++)
   {
      out[i] = temp[i] * det;
   }
   
   return 0;
}

/**
 * Multiplies out matrices for a particular object.
 *
 * This function muliplies together the perspective, camera, and model 
 * matrices for a render operation. All input matrices are optional. You
 * can also pass in an address for the inverse transpose of the model matrix.
 * You can use the inverse transpose to get a matrix that can be used to 
 * transform normals. If the model matrix can not be inverted and the 
 * inverseTranspose parameter is not null then the identity matrix will be 
 * written to the inverseTranspose address.
 * 
 * @param dest              Address to put the result of multiplying together 
 *                          the perspective, camera, and model matrices.
 * @param inverseTranspose  Address to put the inverse transpose of the model
 *                          matrix. This is optional, pass in NULL to ignore.
 * @param perspective       The perspective matrix to use. Can be NULL to ignore.
 * @param camera            The camera matrix to use. Can be NULL to ignore.
 * @param model             The model matrix to use. Can be NULL to ignore.
 * @return                  The value of the dest parameter.
 */
static float * reMat4f_multiplyPCM(float * dest, 
                                   float * inverseTranspose,
                                   const float * perspective, 
                                   const float * camera, 
                                   const float * model)
{
   if(inverseTranspose != NULL)
   {
      reMat4f_identity(inverseTranspose);
      if(model != NULL)
      {
         if(!reMat4f_invert(inverseTranspose, model))
         {
            reMat4f_transpose(inverseTranspose, inverseTranspose);
         }
      }
   }

   if(dest == NULL)
   {
      return NULL;
   }


   if(perspective == NULL)
   {
      reMat4f_identity(dest);
   }
   else
   {
      reMat4f_copy(dest, perspective);
   }

   if(camera != NULL)
   {
      reMat4f_mulitply(dest, dest, camera);
   }
   if(model != NULL)
   {
      reMat4f_mulitply(dest, dest, model);
   }
   return dest;
}




#undef REMAT4F_SET

#endif // __REMAT4F_H__

