#ifndef __RYANSCOPE_H__
#define __RYANSCOPE_H__

struct ryanscope
{
   void ** base;
   unsigned int count;
   unsigned int size;
};

void rsInit(struct ryanscope * rs);

void rsCleanup(struct ryanscope * rs);


#define rsAdd(rs, ...) rsAddUntilNull(rs, __VA_ARGS__, NULL);
void rsAddUntilNull(struct ryanscope * rs, void * data, ...);

void rsAddObj(struct ryanscope * rs, void * data);


#endif // __RYANSCOPE_H__

