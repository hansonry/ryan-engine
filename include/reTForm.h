#ifndef __RETFORM_H__
#define __RETFORM_H__
#include <reMat4f.h>

float * reTForm_setIdentity(float * tform);
float * reTForm_scale(float * tform, float x, float y, float z);
float * reTForm_scaleAll(float * tform, float k);
float * reTForm_translate(float * tform, float x, float y, float z);
float * reTForm_translateVec3f(float * tform, const struct reVec3f * vec);
float * reTForm_translateX(float * tform, float x);
float * reTForm_translateY(float * tform, float y);
float * reTForm_translateZ(float * tform, float z);
float * reTForm_rotateX(float * tform, float radians);
float * reTForm_rotateY(float * tform, float radians);
float * reTForm_rotateZ(float * tform, float radians);
float * reTForm_rotate(float * tform, float radians, float nx, float ny, float nz);
float * reTForm_rotateVec(float * tform, float radians, const struct reVec3f * vec);
float * reTForm_multiplyOnLeft(float * tform, const float * mat);
float * reTForm_multiplyOnLRight(float * tform, const float * mat);
int     reTForm_invert(float * tform);
float * reTForm_transpose(float * tform);
int     reTForm_invertTranspose(float * tform);


#endif // __RETFORM_H__

