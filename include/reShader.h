#ifndef __RESHADER_H__
#define __RESHADER_H__
#include <GL/glew.h>
#include <GL/gl.h>

/**
 * This function loads, compiles and Links an OpenGL shader program
 *
 * This function loads each GLSL text file specifed and attempts to compile
 * it. If compilation was sucessful, this function will then attempt to link
 * all the shaders into a program. If NULL is passed in for one of the
 * parameters, this function will assume that shader will not be used.
 * On failure the function will log the specific error and return 0.
 * 
 * @param vertexShaderFilename   The file path to the vetex shader. NULL can be
 *                               used to disable this shader.
 * @param geometryShaderFilename The file path to the geometry shader. NULL
 *                               can be used to disable this shader.
 * @param fragmentShaderFilename The file path to the fragment shader. NULL
 *                               can be used to disable this shader.
 * @return The OpenGL shader program handle or 0 if something didn't work.
 *
 */
GLuint reShader_loadCompileAndLink(const char * vertexShaderFilename, 
                                   const char * geometryShaderFilename,
                                   const char * fragmentShaderFilename);

/**
 * This function loads and compiles an OpenGL shader program
 *
 * This function loads the specified GLSL text file and attempts to compile
 * it as the specified shader type. The shader should be released after use
 * by glDelteShader.
 * 
 * @param shaderType     The type of shader in the specified file.
 * @param shaderFilename The file contating the GLSL shader to load.
 * @return The OpenGL shader handle or 0 if something didn't work.
 */
GLuint reShader_loadAndCompile(GLenum shaderType, const char * shaderFilename);


/**
 * This function links the passed in OpenGL Program
 *
 * This function assumes all Shaders that are required have been attached.
 * This function does not cleanup any shaders or programs on failure or 
 * success. You will still need to glDeleteProgram or glDeleteShader.
 * This function will log the linked the result in reLog.
 *
 * @param shaderProgramHandle The program handle to link.
 * @return Returns 1 on failure and 0 on success.
 */
int reShader_linkProgram(GLuint shaderProgramHandle);

#endif // __RESHADER_H__

