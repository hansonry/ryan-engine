#ifndef __RYANOBJECT_H__
#define __RYANOBJECT_H__

struct ryanobject
{
   void (*fpDestroy)(struct ryanobject * ro);
};

static inline void roDestroy(struct ryanobject * ro)
{
   ro->fpDestroy(ro);
}

#endif // __RYANOBJECT_H__

