#ifndef __RYANRENDERER_H__
#define __RYANRENDERER_H__
#include <stddef.h>
#include <RyanRendererType.h>
#include <RyanObject.h>

struct ryancolor;
struct ryanbyteinputstream;
struct ryanrenderer;
struct rrprogram;

enum rrdatatype
{
   eRRDT_UInt8,
   eRRDT_SInt8,
   eRRDT_UInt16,
   eRRDT_SInt16,
   eRRDT_UInt32,
   eRRDT_SInt32,
   eRRDT_Float32
};

enum rrbufferupdatehint
{
   eRRBUH_Never,
   eRRBUH_Sometimes,
   eRRBUH_Often,
};

enum rrdrawmode
{
   eRRDM_Points,
   eRRDM_Lines,
   eRRDM_Triangles
};

struct rrattribute
{
   struct ryanobject parent;
   struct rrprogram * (*fpGetProgram)(const struct rrattribute * rra);
   const char * (*fpGetName)(const struct rrattribute * rra);
};

struct rrgeometry
{
   struct ryanobject parent;
   struct ryanrenderer * (*fpGetRenderer)(const struct rrgeometry * rrg);
   struct rrvertexbuffer * (*fpGetVertexBuffer)(const struct rrgeometry * rrg);
   struct rrindexbuffer * (*fpGetIndexBuffer)(const struct rrgeometry * rrg);
   void (*fpAddBufferData)(struct rrgeometry * rrg, 
                           struct rrattribute * rra,
                           size_t sizeInElements,
                           size_t firstOffsetInElements);
   // 0 for indexCount will draw the rest of the buffer
   void (*fpDrawPart)(struct rrgeometry * rrg, size_t startIndex, size_t indexCount);
   void (*fpDraw)(struct rrgeometry * rrg);
};

struct rrvertexbuffer
{
   struct ryanobject parent;
   struct ryanrenderer * (*fpGetRenderer)(const struct rrvertexbuffer * rrvb);
};

struct rrindexbuffer
{
   struct ryanobject parent;
   struct ryanrenderer * (*fpGetRenderer)(const struct rrindexbuffer * rrib);
};

struct rruniform
{
   struct ryanobject parent;
   struct rrprogram * (*fpGetProgram)(const struct rruniform * rru);
   const char * (*fpGetName)(const struct rruniform * rru);
   void (*fpSetMatrix4f)(struct rruniform * rru, float * matrixData, size_t matrixCount);
};

struct rrprogram
{
   struct ryanobject parent;
   struct rrprogram * (*fpGetProgram)(const struct rruniform * rru);
   struct ryanrenderer * (*fpGetRenderer)(const struct rrprogram * rrp);
   struct rruniform * (*fpCreateUniformFromName)(struct rrprogram * rrp, const char * name);
   struct rruniform * (*fpCreateUniformFromIndex)(struct rrprogram * rrp, unsigned int index);
   struct rrattribute * (*fpCreateAttributeFromName)(struct rrprogram * rrp, const char * name);
   struct rrattribute * (*fpCreateAttributeFromIndex)(struct rrprogram * rrp, unsigned int index);
   void (*fpUse)(struct rrprogram * rrp);
};


struct ryanrenderer
{
   struct ryanobject parent;
   struct rrprogram * (*fpGetProgram)(const struct rruniform * rru);
   enum ryanrenderertype (*fpGetRendererType)(const struct ryanrenderer * rr);
   void (*fpSetClearColor)(struct ryanrenderer * rr, 
                           const struct ryancolor * color);
   void (*fpClearColor)(struct ryanrenderer * rr);
   void (*fpClearDepth)(struct ryanrenderer * rr);
   void (*fpClearColorAndDepth)(struct ryanrenderer * rr);
   struct rrprogram * (*fpCreateProgramFromStreams)(struct ryanrenderer * rr,
                                                    struct ryanbyteinputstream * vertexStream,
                                                    struct ryanbyteinputstream * geometryStream,
                                                    struct ryanbyteinputstream * fragmentStream);
   struct rrvertexbuffer * (*fpCreateVertexBuffer)(struct ryanrenderer * rr, 
                                                   enum rrdatatype type, 
                                                   const void * data, 
                                                   size_t sizeInElements,
                                                   enum rrbufferupdatehint updateHint);
   struct rrindexbuffer * (*fpCreateIndexBuffer)(struct ryanrenderer * rr, 
                                                 enum rrdatatype type, 
                                                 const void * data,
                                                 size_t sizeInElements,
                                                 enum rrbufferupdatehint updateHint);
   struct rrgeometry * (*fpCreateGeometry)(struct ryanrenderer * rr,
                                           struct rrvertexbuffer * vertexBuffer,
                                           size_t vertexBufferStrideInElements,
                                           struct rrindexbuffer * indexBuffer,
                                           enum rrdrawmode drawMode);
};

struct rrprogram * rrCreateProgramFromFiles(struct ryanrenderer * rr,
                                            const char * vertexFilename,
                                            const char * geometryFilename,
                                            const char * fragmentFilename);

struct rrprogram * rrCreateProgramFromStrings(struct ryanrenderer * rr,
                                              const char * vertexCode,
                                              const char * geometryCode,
                                              const char * fragmentCode);


#define RR_INLINE_DECLARE
#include <RyanVirtualInlineMacro.h>
#undef RR_INLINE_DECLARE

#define CT struct ryanrenderer *

DECLARE_DESTROY_FUNCTION(  CT, rr)
DECLARE_INLINE_FUNCTION_0( const CT, rr, GetRendererType)

DECLARE_INLINE_FUNCTION_1( CT, rr, SetClearColor, const struct ryancolor *)

DECLARE_INLINE_FUNCTION_0( CT, rr, ClearColor)
DECLARE_INLINE_FUNCTION_0( CT, rr, ClearDepth)
DECLARE_INLINE_FUNCTION_0( CT, rr, ClearColorAndDepth)
DECLARE_INLINE_FUNCTION_3R(CT, rr, CreateProgramFromStreams, struct rrprogram * , 
                                                             struct ryanbyteinputstream * , 
                                                             struct ryanbyteinputstream * , 
                                                             struct ryanbyteinputstream * ) 
   
DECLARE_INLINE_FUNCTION_4R(CT, rr, CreateVertexBuffer, struct rrvertexbuffer * , 
                                                       enum rrdatatype,
                                                       const void *,
                                                       size_t, 
                                                       enum rrbufferupdatehint) 
DECLARE_INLINE_FUNCTION_4R(CT, rr, CreateIndexBuffer, struct rrindexbuffer * , 
                                                      enum rrdatatype,
                                                      const void *,
                                                      size_t, 
                                                      enum rrbufferupdatehint) 

DECLARE_INLINE_FUNCTION_4R(CT, rr, CreateGeometry, struct rrgeometry * , 
                                                   struct rrvertexbuffer * ,
                                                   size_t,
                                                   struct rrindexbuffer *,
                                                   enum rrdrawmode) 

#undef CT

#define CT struct rrprogram *

DECLARE_DESTROY_FUNCTION(  CT, rrp)
DECLARE_INLINE_FUNCTION_0R(const CT, rrp, GetRenderer, struct ryanrenderer *)
DECLARE_INLINE_FUNCTION_1R(CT, rrp, CreateUniformFromName,  struct rruniform *, const char *)
DECLARE_INLINE_FUNCTION_1R(CT, rrp, CreateUniformFromIndex, struct rruniform *, unsigned int)
DECLARE_INLINE_FUNCTION_1R(CT, rrp, CreateAttributeFromName,  struct rrattribute *, const char *)
DECLARE_INLINE_FUNCTION_1R(CT, rrp, CreateAttributeFromIndex, struct rrattribute *, unsigned int)
DECLARE_INLINE_FUNCTION_0( CT, rrp, Use)
#undef CT

#define CT struct rruniform *

DECLARE_DESTROY_FUNCTION(  CT, rru)
DECLARE_INLINE_FUNCTION_0R(const CT, rru, GetProgram, struct rrprogram *)
DECLARE_INLINE_FUNCTION_0R(const CT, rru, GetName, const char *)
DECLARE_INLINE_FUNCTION_2( CT, rru, SetMatrix4f, float * , size_t)


#undef CT

#define CT struct rrvertexbuffer *

DECLARE_DESTROY_FUNCTION(  CT, rrvb)
DECLARE_INLINE_FUNCTION_0R(const CT, rrvb, GetRenderer, struct ryanrenderer *)

#undef CT

#define CT struct rrindexbuffer *

DECLARE_DESTROY_FUNCTION(  CT, rrib)
DECLARE_INLINE_FUNCTION_0R(const CT, rrib, GetRenderer, struct ryanrenderer *)

#undef CT

#define CT struct rrgeometry *

DECLARE_DESTROY_FUNCTION(  CT, rrg)
DECLARE_INLINE_FUNCTION_0R(const CT, rrg, GetRenderer, struct ryanrenderer *)
DECLARE_INLINE_FUNCTION_0R(const CT, rrg, GetVertexBuffer, struct rrvertexbuffer *)
DECLARE_INLINE_FUNCTION_0R(const CT, rrg, GetIndexBuffer, struct rrindexbuffer *)
DECLARE_INLINE_FUNCTION_3( CT, rrg, AddBufferData, struct rrattribute *,
                                                   size_t, size_t)
DECLARE_INLINE_FUNCTION_2( CT, rrg, DrawPart, size_t, size_t)
DECLARE_INLINE_FUNCTION_0( CT, rrg, Draw)

#undef CT

#define CT struct rrattribute *

DECLARE_DESTROY_FUNCTION(  CT, rra)
DECLARE_INLINE_FUNCTION_0R(const CT, rra, GetProgram, struct rrprogram *)
DECLARE_INLINE_FUNCTION_0R(const CT, rra, GetName, const char *)


#undef CT

#include <RyanVirtualInlineMacro.h>

#endif // __RYANRENDERER_H__

