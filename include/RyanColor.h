#ifndef __RYANCOLOR_H__
#define __RYANCOLOR_H__
#include <stdint.h>

struct ryancolor
{
   float r;
   float g;
   float b;
   float a;
};


extern const float            rcAlphaOpaque;
extern const struct ryancolor rcColorWhite;
extern const struct ryancolor rcColorBlack;
extern const struct ryancolor rcColorGrey;
extern const struct ryancolor rcColorRed;
extern const struct ryancolor rcColorGreen;
extern const struct ryancolor rcColorBlue;
extern const struct ryancolor rcColorYellow;
extern const struct ryancolor rcColorCyan;
extern const struct ryancolor rcColorMagenta;
extern const struct ryancolor rcColorDarkRed;
extern const struct ryancolor rcColorDarkGreen;
extern const struct ryancolor rcColorDarkBlue;
extern const struct ryancolor rcColorDarkYellow;
extern const struct ryancolor rcColorDarkCyan;
extern const struct ryancolor rcColorDarkMagenta;
extern const struct ryancolor rcColorBlueBackground;

static inline void rcSetWithAlpha(struct ryancolor * color, 
                                  float red, 
                                  float green, 
                                  float blue,
                                  float alpha)
{
   color->r = red;
   color->g = green;
   color->b = blue;
   color->a = alpha;
}

static inline void rcSet(struct ryancolor * color, 
                         float red, 
                         float green, 
                         float blue)
{
   color->r = red;
   color->g = green;
   color->b = blue;
   color->a = 1.0f;
}

static inline void rcCopy(struct ryancolor * dest, 
                          const struct ryancolor * src)
{
   dest->r = src->r;
   dest->g = src->g;
   dest->b = src->b;
   dest->a = src->a;
}

static inline void rcSetFromInt(struct ryancolor * color, 
                                uint32_t value)
{
   if(value > 0xFFFFFF)
   {
      color->r = ((value >> 24) & 0xFF) / 255.0f;
      color->g = ((value >> 16) & 0xFF) / 255.0f;
      color->b = ((value >> 8)  & 0xFF) / 255.0f;
      color->a = ( value        & 0xFF) / 255.0f;
   }
   else
   {
      color->r = ((value >> 16) & 0xFF) / 255.0f;
      color->g = ((value >> 8)  & 0xFF) / 255.0f;
      color->b = ( value        & 0xFF) / 255.0f;
      color->a = 1.0f;
   }
}


#endif // __RYANCOLOR_H__


