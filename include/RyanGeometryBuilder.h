#ifndef __RYANGEOMETRYBUILDER_H__
#define __RYANGEOMETRYBUILDER_H__
#include <stddef.h>
#include <stdbool.h>

#include <RyanRenderer.h>



struct rgbVertexID;
struct ryangeometrybuilder
{
   struct rgbVertexID * vertexIDListBase;
   size_t vertexIDListCount;
   size_t vertexSize;
   double * vertexBufferBase;
   double * vertexListBase;
   size_t vertexListSize;
   size_t vertexListCount;
   unsigned int * indexListBase;
   size_t indexListSize;
   size_t indexListCount;
};

// vertexFormat example: "vertex[3] color[4] normal[4]"
void rgbInit(struct ryangeometrybuilder * rgb, const char * vertexFormat);
void rgbCleanup(struct ryangeometrybuilder * rgb);

bool rgbSetVertexValue(struct ryangeometrybuilder * rgb, const char * name,  ...);
unsigned int rgbAddVertex(struct ryangeometrybuilder * rgb);

void rgbAddIndex(struct ryangeometrybuilder * rgb, unsigned int index);
unsigned int rgbAddVertexAndIndex(struct ryangeometrybuilder * rgb);

bool rgbSetProgramAttribute(struct ryangeometrybuilder * rgb, 
                            const char * name, struct rrattribute * rra);


struct rrgeometry * rgbCreateRenderableGeometry(struct ryangeometrybuilder * rgb,
                                                struct ryanrenderer * rr,
                                                enum rrdatatype vertexType,
                                                enum rrdatatype indexType,
                                                enum rrdrawmode drawMode);

void rgbClearData(struct ryangeometrybuilder * rgb);
#endif // __RYANGEOMETRYBUILDER_H__
