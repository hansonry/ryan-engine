#ifndef __RESTATETREE_H__
#define __RESTATETREE_H__
#include <SDL.h>

typedef void (*reStateTree_callbackLoad)(void * object);
typedef void (*reStateTree_callbackUnload)(void * object);

typedef void (*reStateTree_callbackEnable)(void * object);
typedef void (*reStateTree_callbackDisable)(void * object);
typedef void (*reStateTree_callbackEvent)(void * object, SDL_Event * event);
typedef void (*reStateTree_callbackUpdate)(void * object, float seconds);
typedef void (*reStateTree_callbackRender)(void * object);

struct reStateTree;

struct reStateTreeNode;

struct reStateTreeNodeData
{
   void                      * object;
   reStateTree_callbackLoad    cbLoad;
   reStateTree_callbackUnload  cbUnload;
   reStateTree_callbackEnable  cbEnable;
   reStateTree_callbackDisable cbDisable;
   reStateTree_callbackEvent   cbEvent;
   reStateTree_callbackUpdate  cbUpdate;
   reStateTree_callbackRender  cbRender;
};

typedef void (*reStateTree_callbackCreate)(struct reStateTreeNode * node, struct reStateTreeNodeData * data);

struct reStateTree * reStateTree_create(reStateTree_callbackCreate rootNodeCallback);
void reStateTree_destroy(struct reStateTree * tree);

struct reStateTreeNode * reStateTree_getRootNode(struct reStateTree * tree);

void reStateTree_runLoad(struct reStateTree * tree);
void reStateTree_runUnload(struct reStateTree * tree);
void reStateTree_runEvent(struct reStateTree * tree, SDL_Event * event);
void reStateTree_runUpdate(struct reStateTree * tree, float seconds);
void reStateTree_runRender(struct reStateTree * tree); 
void reStateTree_manageState(struct reStateTree * tree);

void reStateTree_enableNode(struct reStateTreeNode * node);
void reStateTree_disableNode(struct reStateTreeNode * node);


struct reStateTreeNode * reStateTree_addNode(struct reStateTreeNode * parent, 
                                             void * object, 
                                             reStateTree_callbackCreate callback);



#endif // __RESTATETREE_H__
