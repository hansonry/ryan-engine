#ifndef __RYANIO_H__
#define __RYANIO_H__
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <RyanObject.h>

struct ryanbyteinputstream 
{
   struct ryanobject parent;
   size_t (*fpRead)(struct ryanbyteinputstream * rbis, void * buffer, size_t bufferSize);
   bool (*fpIsEndOfStream)(struct ryanbyteinputstream * rbis);
   const char * id; // This is used to id the stream for log messages
};

struct ryanbyteinputstream * rbisCreateFromBuffer(const void * buffer, size_t size);
struct ryanbyteinputstream * rbisCreateFromBufferOwned(void * buffer, size_t size);
struct ryanbyteinputstream * rbisCreateFromString(const char * string);
struct ryanbyteinputstream * rbisCreateFromStringOwned(char * string);
struct ryanbyteinputstream * rbisCreateFromFile(const char * filename);
struct ryanbyteinputstream * rbisCreateFromFileHandle(FILE * fileHandle);


void * rbisDumpToMemory(struct ryanbyteinputstream * rbis, size_t * readSize, size_t growby);

#define RR_INLINE_DECLARE
#include <RyanVirtualInlineMacro.h>
#undef RR_INLINE_DECLARE

#define CT struct ryanbyteinputstream *

DECLARE_DESTROY_FUNCTION(CT, rbis);

DECLARE_INLINE_FUNCTION_2R(CT, rbis, Read, size_t, void *, size_t)

DECLARE_INLINE_FUNCTION_0R(CT, rbis, IsEndOfStream, bool)

#undef CT

#include <RyanVirtualInlineMacro.h>



#endif // __RYANIO_H__

