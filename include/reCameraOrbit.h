#ifndef __RECAMERAORBIT_H__
#define __RECAMERAORBIT_H__
#include <reStateTree.h>
#include <reVec3f.h>
#include <reMat4f.h>
#include <SDL.h>

struct reCameraOrbit
{
   float distance;
   float targetDistance;
   float yaw;
   float pitch;
   struct reVec3f origin;
};


struct reStateTreeNode * reCameraOrbit_addNode(struct reCameraOrbit * cam, 
                                               struct reStateTreeNode * node);

void reCameraOrbit_setAll(struct reCameraOrbit * cam, 
                          float ox, float oy, float oz, 
                          float distance, float yaw, float pitch);

void reCameraOrbit_setOrigin(struct reCameraOrbit * cam,
                             float ox, float oy, float oz);


void reCameraOrbit_update(struct reCameraOrbit * cam, float update);

void reCameraOrbit_event(struct reCameraOrbit * cam,
                         SDL_Event * event);

float * reCameraOrbit_computeMatrix(const struct reCameraOrbit * cam, 
                                    float * mat); 


#endif // __RECAMERAORBIT_H__

