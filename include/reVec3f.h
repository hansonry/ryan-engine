#ifndef __REVEC3F_H__
#define __REVEC3F_H__
#include <math.h>

struct reVec3f
{
   float x;
   float y;
   float z;
};


static inline struct reVec3f * reVec3f_set(struct reVec3f * vec, float x, float y, float z)
{
   vec->x = x;
   vec->y = y;
   vec->z = z;
   return vec;
}

static inline struct reVec3f * reVec3f_setFromArray(unsigned int count, struct reVec3f * vec, const float * array)
{
   unsigned int i;
   for(i = 0; i < count; i++)
   {
      vec[i].x = array[0];
      vec[i].y = array[1];
      vec[i].z = array[2];
      array += 3;
   }
   return vec;
}

static inline struct reVec3f * reVec3f_copy(struct reVec3f * dest, const struct reVec3f  * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}

static inline struct reVec3f * reVec3f_setZero(struct reVec3f * vec)
{
   vec->x = 0;
   vec->y = 0;
   vec->z = 0;
   return vec;
}

static inline struct reVec3f * reVec3f_setUnitX(struct reVec3f * vec)
{
   vec->x = 1;
   vec->y = 0;
   vec->z = 0;
   return vec;
}

static inline struct reVec3f * reVec3f_setUnitY(struct reVec3f * vec)
{
   vec->x = 0;
   vec->y = 1;
   vec->z = 0;
   return vec;
}

static inline struct reVec3f * reVec3f_setUnitZ(struct reVec3f * vec)
{
   vec->x = 0;
   vec->y = 0;
   vec->z = 1;
   return vec;
}

static inline struct reVec3f * reVec3f_invert(struct reVec3f * vec)
{
   vec->x = -vec->x;
   vec->y = -vec->y;
   vec->z = -vec->z;
   return vec;
}

static inline struct reVec3f * reVec3f_scale(struct reVec3f * vec, float k)
{
   vec->x *= k;
   vec->y *= k;
   vec->z *= k;
   return vec;
}

static inline struct reVec3f * reVec3f_add(struct reVec3f * vec, float x, float y, float z)
{
   vec->x += x;
   vec->y += y;
   vec->z += z;
   return vec;
}

static inline struct reVec3f * reVec3f_addVec(struct reVec3f * vec, const struct reVec3f * toAdd)
{
   vec->x += toAdd->x;
   vec->y += toAdd->y;
   vec->z += toAdd->z;
   return vec;
}

static inline struct reVec3f * reVec3f_subtract(struct reVec3f * vec, float x, float y, float z)
{
   vec->x -= x;
   vec->y -= y;
   vec->z -= z;
   return vec;
}

static inline struct reVec3f * reVec3f_subtractVec(struct reVec3f * vec, const struct reVec3f * toAdd)
{
   vec->x -= toAdd->x;
   vec->y -= toAdd->y;
   vec->z -= toAdd->z;
   return vec;
}

static inline float reVec3f_length2(const struct reVec3f * vec)
{
   return vec->x * vec->x + vec->y * vec->y + vec->z * vec->z;
}

static inline float reVec3f_length(const struct reVec3f * vec)
{
   return sqrt(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
}

static inline float reVec3f_dot(const struct reVec3f * a, const struct reVec3f * b)
{
   return a->x * b->x + a->y * b->y + a->z * b->z;
}

static inline struct reVec3f * reVec3f_cross(struct reVec3f * reg, const struct reVec3f * b)
{
   struct reVec3f a;
   a.x = reg->x;
   a.y = reg->y;
   a.z = reg->z;

   reg->x = a.y * b->z - a.z * b->y;
   reg->y = a.z * b->x - a.x * b->z;
   reg->z = a.x * b->y - a.y * b->x;
   return reg;
}

static inline void reVec3f_putInArray(unsigned int count, float * array, const struct reVec3f * vec)
{
   unsigned int i;
   for(i = 0; i < count; i++)
   {
      array[0] = vec[i].x;
      array[1] = vec[i].y;
      array[2] = vec[i].z;
      array += 3;
   }
}

static inline struct reVec3f * reVec3f_addScaled(struct reVec3f * vec, float scale, float x, float y, float z)
{
   vec->x += scale * x;
   vec->y += scale * y;
   vec->z += scale * z;
   return vec;
}

static inline struct reVec3f * reVec3f_addScaledVec(struct reVec3f * vec, float scale, const struct reVec3f * b)
{
   vec->x += scale * b->x;
   vec->y += scale * b->y;
   vec->z += scale * b->z;
   return vec;
}

static inline struct reVec3f * reVec3f_normalize(struct reVec3f * vec)
{
   float length = reVec3f_length(vec);
   if(length < 0.0000001f)
   {
      vec->x = 1;
      vec->y = 0;
      vec->z = 0;
   }
   else
   {
      vec->x /= length;
      vec->y /= length;
      vec->z /= length;
   }
   return vec;
}

#endif  // __REVEC3F_H__


