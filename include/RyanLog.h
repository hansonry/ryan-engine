#ifndef __RL_H__
#define __RL_H__
#include <stdarg.h>

#define RL_LEVEL_CRITICAL 0
#define RL_LEVEL_ERROR    1
#define RL_LEVEL_WARNING  2
#define RL_LEVEL_INFO     3
#define RL_LEVEL_DEBUG    4
#define RL_LEVEL_TRACE    5

#ifndef RL_MAX_LEVEL
#define RL_MAX_LEVEL  RL_LEVEL_DEBUG
#endif


void rlInit(void);
void rlCleanup(void);

int rlSetMaxLevel(int level);
int rlSetMaxFileLevel(int level);
int rlSetMaxConsoleLevel(int level);

void rlLogV(const char * filename, int line, int level, const char * msgfmt, va_list args);
void rlLog(const char * filename, int line, int level, const char * msgfmt, ...);


#if RL_LEVEL_CRITICAL <= RL_MAX_LEVEL 
#define rlCritical(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_CRITICAL, __VA_ARGS__)
#else
#define rlCritical(msgfmt, ...) 
#endif

#if RL_LEVEL_ERROR <= RL_MAX_LEVEL 
#define rlError(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_ERROR, __VA_ARGS__)
#else
#define rlError(msgfmt, ...) 
#endif

#if RL_LEVEL_WARNING <= RL_MAX_LEVEL 
#define rlWarning(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_WARNING, __VA_ARGS__)
#else
#define rlWarning(...) 
#endif

#if RL_LEVEL_DEBUG <= RL_MAX_LEVEL 
#define rlDebug(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_DEBUG, __VA_ARGS__)
#else
#define rlDebug(...) 
#endif

#if RL_LEVEL_INFO <= RL_MAX_LEVEL 
#define rlInfo(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_INFO, __VA_ARGS__)
#else
#define rlInfo(...) 
#endif

#if RL_LEVEL_TRACE <= RL_MAX_LEVEL 
#define rlTrace(...) \
rlLog(__FILE__, __LINE__, RL_LEVEL_TRACE, __VA_ARGS__)
#else
#define rlTrace(...) 
#endif

#endif // __RL_H__

