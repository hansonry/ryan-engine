#ifndef __RESHAPE_H_
#define __RESHAPE_H_
#include <GL/gl.h>

struct reShape
{
   GLuint  vertexArrayObject;
   GLuint  arrayVertexBufferObject;
   GLuint  elementArrayVertexBufferObject;
   GLenum  mode;
   GLsizei count;
};

void reShape_teardown(struct reShape * shape);

static inline void reShape_draw(const struct reShape * shape)
{
   glBindVertexArray(shape->vertexArrayObject);
   glDrawElements(shape->mode, shape->count, GL_UNSIGNED_SHORT, (void*)0);
   glBindVertexArray(0);
}


// Shapes

/// 2D Shapes (All 2d Shapes live in the x/y plane with z being 0)

void reShape_circle(struct reShape * shape, float xCenter, float yCenter, 
                    float radius, unsigned int points);
void reShape_pie(struct reShape * shape, float xCenter, float yCenter, 
                 float radius, unsigned int points, float startRadians, 
                 float endRadians);
                 
void reShape_rectangle(struct reShape * shape, float x1, float y1, 
                                               float x2, float y2);

/// 3D Shapes

void reShape_box(struct reShape * shape, float x1, float y1, float z1, 
                                         float x2, float y2, float z2);
void reShape_cube(struct reShape * shape);

/*
void reShape_cylinder(struct reShape * shape, float height, 
                                              float topRadius, 
                                              float bottomRadius,
                                              unsigned int sides);
*/



#endif // __RESHAPE_H_

