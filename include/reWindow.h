#ifndef __REWINDOW_H__
#define __REWINDOW_H__
#include <SDL.h>
#include <reStateTree.h>



typedef void (*reWindow_callbackCreated)(SDL_Window * window, 
                                         int viewPortWidth, 
                                         int viewPortHeight);


void reWindow_setCreatedCallback(reWindow_callbackCreated callback);
void reWindow_setCloseOnXorESC(int flag);
SDL_Window * reWindow_getSDLHandle(void);
void reWindow_getSize(int * width, int * height);



void reWindow_stop(void);

/**
 * Launches a new wind and runs the event loop.
 * 
 * Creates an OpenGL enabled SDL window for opengl rendering. This also 
 * handles setup and teardown of the Ryan Engine System. All the parameters
 * in this function are SDL parameters so you can use them how SDL
 * would let you use them. It should be noted that this function will
 * not return until the event loop is finished (aka game is ended).
 *
 * @param title The window title. This is passed to SDL_CreateWindow.
 * @param x     The window x location on the screen. This is passed to SDL_CreateWindow.
 * @param y     The window y location on the screen. This is passed to SDL_CreateWindow.
 * @param w     The width of the window. This is passed to SDL_CreateWindow.
 * @param h     The height of the window. This is passed to SDL_CreateWindow.
 * @param flags The SDL flags that are passed to SDL_CreateWindow. Note:
 *              the flags SDL_WINDOW_SHOW and SDL_WINDOW_OPENGL will be
 *              appended to this parameter.
 * @return 0 on sucess and 1 on failure.
 *
 */
int  reWindow_run(const char * title, int x, int y, int w, int h, Uint32 flags,
                  reStateTree_callbackCreate applicationCallback);
int  reWindow_runDefaults(reStateTree_callbackCreate applicationCallback);


#endif // __REWINDOW_H__

