#ifndef __REGLBINDINGS_H__
#define __REGLBINDINGS_H__
#include <GL/glew.h>
#include <GL/gl.h>
#include <reMat4f.h>

// === reMat3f === //
static inline void reMat4f_setUniform(GLint location, GLsizei count, const float * mats)
{
   glUniformMatrix4fv(location, count, GL_FALSE, mats);
}

#endif // __REGLBINDINGS_H__
