
#ifdef RR_INLINE_DECLARE

#define DECLARE_DESTROY_FUNCTION(ct, pref) \
static inline void pref##Destroy (ct c)    \
{                                          \
   roDestroy((struct ryanobject * )c);     \
}

#define DECLARE_INLINE_FUNCTION_0(ct, pref, name)                \
static inline void pref##name (ct c)                             \
{                                                                \
   c->fp##name (c);                                              \
}

#define DECLARE_INLINE_FUNCTION_0R(ct, pref, name, rett)         \
static inline rett pref##name (ct c)                             \
{                                                                \
   return c->fp##name (c);                                       \
}

#define DECLARE_INLINE_FUNCTION_1(ct, pref, name,                \
                                  pt1)                           \
static inline void pref##name (ct c, pt1 p1)                     \
{                                                                \
   c->fp##name (c, p1);                                          \
}

#define DECLARE_INLINE_FUNCTION_1R(ct, pref, name, rett,         \
                                  pt1)                           \
static inline rett pref##name (ct c, pt1 p1)                     \
{                                                                \
   return c->fp##name (c, p1);                                   \
}

#define DECLARE_INLINE_FUNCTION_2(ct, pref, name,                \
                                  pt1, pt2)                      \
static inline void pref##name (ct c, pt1 p1, pt2 p2)             \
{                                                                \
   c->fp##name (c, p1, p2);                                      \
}

#define DECLARE_INLINE_FUNCTION_2R(ct, pref, name, rett,         \
                                  pt1, pt2)                      \
static inline rett pref##name (ct c, pt1 p1, pt2 p2)             \
{                                                                \
   return c->fp##name (c, p1, p2);                               \
}

#define DECLARE_INLINE_FUNCTION_3(ct, pref, name,                \
                                  pt1, pt2, pt3)                 \
static inline void pref##name (ct c, pt1 p1, pt2 p2, pt3 p3)     \
{                                                                \
   c->fp##name (c, p1, p2, p3);                                  \
}

#define DECLARE_INLINE_FUNCTION_3R(ct, pref, name, rett,         \
                                  pt1, pt2, pt3)                 \
static inline rett pref##name (ct c, pt1 p1, pt2 p2, pt3 p3)     \
{                                                                \
   return c->fp##name (c, p1, p2, p3);                           \
}

#define DECLARE_INLINE_FUNCTION_4(ct, pref, name,                \
                                  pt1, pt2, pt3, pt4)            \
static inline void pref##name (ct c, pt1 p1, pt2 p2, pt3 p3,     \
                                     pt4 p4)                     \
{                                                                \
   c->fp##name (c, p1, p2, p3, p4);                              \
}

#define DECLARE_INLINE_FUNCTION_4R(ct, pref, name, rett,         \
                                  pt1, pt2, pt3, pt4)            \
static inline rett pref##name (ct c, pt1 p1, pt2 p2, pt3 p3,     \
                                     pt4 p4)                     \
{                                                                \
   return c->fp##name (c, p1, p2, p3, p4);                       \
}

#else // RR_INLINE_DECLARE

#undef DECLARE_DESTROY_FUNCTION
#undef DECLARE_INLINE_FUNCTION_0
#undef DECLARE_INLINE_FUNCTION_0R
#undef DECLARE_INLINE_FUNCTION_1
#undef DECLARE_INLINE_FUNCTION_1R
#undef DECLARE_INLINE_FUNCTION_2
#undef DECLARE_INLINE_FUNCTION_2R
#undef DECLARE_INLINE_FUNCTION_3
#undef DECLARE_INLINE_FUNCTION_3R
#undef DECLARE_INLINE_FUNCTION_4
#undef DECLARE_INLINE_FUNCTION_4R

#endif // RR_INLINE_DECLARE

