#include <reTForm.h>


float * reTForm_setIdentity(float * tform)
{
   return reMat4f_identity(tform);
}

float * reTForm_scale(float * tform, float x, float y, float z)
{
   float mat[REMAT4F_SIZE];
   reMat4f_scale(mat, x, y, z);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_scaleAll(float * tform, float k)
{
   float mat[REMAT4F_SIZE];
   reMat4f_scaleAll(mat, k);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_translate(float * tform, float x, float y, float z)
{
   float mat[REMAT4F_SIZE];
   reMat4f_translate(mat, x, y, z);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_translateVec3f(float * tform, const struct reVec3f * vec)
{
   float mat[REMAT4F_SIZE];
   reMat4f_translateVec3f(mat, vec);
   return reMat4f_mulitply(tform, tform, mat);
}
float * reTForm_translateX(float * tform, float x)
{
   float mat[REMAT4F_SIZE];
   reMat4f_translate(mat, x, 0, 0);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_translateY(float * tform, float y)
{
   float mat[REMAT4F_SIZE];
   reMat4f_translate(mat, 0, y, 0);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_translateZ(float * tform, float z)
{
   float mat[REMAT4F_SIZE];
   reMat4f_translate(mat, 0, 0, z);
   return reMat4f_mulitply(tform, tform, mat);
}


float * reTForm_rotateX(float * tform, float radians)
{
   float mat[REMAT4F_SIZE];
   reMat4f_rotateX(mat, radians);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_rotateY(float * tform, float radians)
{
   float mat[REMAT4F_SIZE];
   reMat4f_rotateY(mat, radians);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_rotateZ(float * tform, float radians)
{
   float mat[REMAT4F_SIZE];
   reMat4f_rotateZ(mat, radians);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_rotate(float * tform, float radians, float nx, float ny, float nz)
{
   float mat[REMAT4F_SIZE];
   reMat4f_rotate(mat, radians, nx, ny, nz);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_rotateVec(float * tform, float radians, const struct reVec3f * vec)
{
   float mat[REMAT4F_SIZE];
   reMat4f_rotateVec(mat, radians, vec);
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_multiplyOnLeft(float * tform, const float * mat)
{
   return reMat4f_mulitply(tform, tform, mat);
}

float * reTForm_multiplyOnRight(float * tform, const float * mat)
{
   return reMat4f_mulitply(tform, mat, tform);
}

int     reTForm_invert(float * tform)
{
   return reMat4f_invert(tform, tform);
}

float * reTForm_transpose(float * tform)
{
   return reMat4f_transpose(tform, tform);
}

int     reTForm_invertTranspose(float * tform)
{
   if(reMat4f_invert(tform, tform))
   {
      return 1;
   }
   reMat4f_transpose(tform, tform);
   return 0;
}

