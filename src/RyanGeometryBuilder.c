#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <RyanGeometryBuilder.h>
#include <RyanLog.h>

#define DEFAULT_GROWBY 64

struct rgbVertexID
{
   char * name;
   size_t size;
   size_t startsAt;
   struct rrattribute * rra;
};

static bool rgbParseNameFromFormat(const char * vertexFormat, size_t * index, 
                                   struct rgbVertexID * id)
{
   size_t startIndex;
   size_t stringSize;
   
   // Ignore Whitespace
   while(vertexFormat[*index] == ' ' || vertexFormat[*index] == '\t' ||
         vertexFormat[*index] == 'r' || vertexFormat[*index] == '\n')
   {
      (*index) ++;
   }
   if(vertexFormat[*index] == '\0' || vertexFormat[*index] == '[' ||
      vertexFormat[*index] == ']')
   {
      return false;
   }
   
   startIndex = *index;
   
   while(vertexFormat[*index] != '\0' && vertexFormat[*index] != '[' &&
         vertexFormat[*index] != ']')
   {
      (*index) ++;
   }
   
   if(startIndex >= *index || vertexFormat[*index] == ']')
   {
      return false;
   }
   
   // copy out the string
   stringSize = *index - startIndex;
   id->name = malloc(stringSize + 1);
   memcpy(id->name, &vertexFormat[startIndex], stringSize);
   id->name[stringSize] = '\0';
   
   return true;
}

static bool rgbParseSizeFromFormat(const char * vertexFormat, size_t * index,
                                   struct rgbVertexID * id)
{
   size_t startIndex;
   if(vertexFormat[*index] != '[')
   {
      return false;
   }
   (*index) ++;
   startIndex = *index;
   while(vertexFormat[*index] >= '0' && vertexFormat[*index] <= '9')
   {
      (*index) ++;
   }
   
   if(vertexFormat[*index] != ']')
   {
      return false;
   }
   id->size = (size_t)strtol(&vertexFormat[startIndex], NULL, 10);
   
   return true;
}

static inline bool rgbParseVertexIDFromFormat(const char * vertexFormat, size_t * index, 
                                              struct rgbVertexID * id)
{
   if(!rgbParseNameFromFormat(vertexFormat, index, id))
   {
      return false;
   }
   
   if(!rgbParseSizeFromFormat(vertexFormat, index, id))
   {
      return false;
   }
   
   return true;
}

static void rgbParseVertexFormat(struct ryangeometrybuilder * rgb, const char * vertexFormat)
{
   struct rgbVertexID localIDStorage;
   size_t index = 0;
   size_t vertexIDListSize = 0;
   size_t startsAt = 0;
   
   rlDebug("Parsing Vertex Format \"%s\"", vertexFormat);
   // Initialize vertexIDList 
   rgb->vertexIDListCount = 0;
   rgb->vertexIDListBase  = NULL;
   
   // Keep parsing and adding ids
   while(rgbParseVertexIDFromFormat(vertexFormat, &index, &localIDStorage))
   {
      // Make room if nessary
      if(rgb->vertexIDListCount >= vertexIDListSize)
      {
         vertexIDListSize = rgb->vertexIDListCount + 8;
         rgb->vertexIDListBase = realloc(rgb->vertexIDListBase, 
                                         sizeof(struct rgbVertexID) *  vertexIDListSize);
      }
      // Set and Compute StartsAt
      localIDStorage.startsAt = startsAt;
      startsAt += localIDStorage.size;
      
      localIDStorage.rra = NULL;
      
      rlDebug("Parsing Vertex Format Found Entry \"%s\", with (start, size) of (%d, %d)", 
              localIDStorage.name, localIDStorage.startsAt, localIDStorage.size);
      
      // Add new id to the end of the list
      memcpy(&rgb->vertexIDListBase[rgb->vertexIDListCount], 
             &localIDStorage, sizeof(struct rgbVertexID));
      rgb->vertexIDListCount ++;
   }
}

static inline void rgbClearVertexBuffer(struct ryangeometrybuilder * rgb)
{
   size_t i;
   for(i = 0; i < rgb->vertexSize; i++)
   {
      rgb->vertexBufferBase[i] = 0.0f;
   }
}

// vertexFormat example: "vertex[3] color[4] normal[4]"
void rgbInit(struct ryangeometrybuilder * rgb, const char * vertexFormat)
{
   struct rgbVertexID * lastId;

   rgbParseVertexFormat(rgb, vertexFormat);
   
   // Compute Vertex Size in elements
   if(rgb->vertexIDListCount > 0)
   {
      lastId = &rgb->vertexIDListBase[rgb->vertexIDListCount - 1];
      rgb->vertexSize = lastId->startsAt + lastId->size;
      
      // Create the vertex buffer
      rgb->vertexBufferBase = malloc(sizeof(double) * rgb->vertexIDListCount);
      
      rgbClearVertexBuffer(rgb);
   }
   else
   {
      rgb->vertexSize = 0;
      rgb->vertexBufferBase = NULL;
   }
   

   
   // Initialize the lists
   rgb->vertexListBase  = NULL;
   rgb->vertexListSize  = 0;
   rgb->vertexListCount = 0;
   rgb->indexListBase   = NULL;
   rgb->indexListSize   = 0;
   rgb->indexListCount  = 0;
}

void rgbCleanup(struct ryangeometrybuilder * rgb)
{
   size_t i;
   
   for(i = 0; i < rgb->vertexIDListCount; i++)
   {
      free(rgb->vertexIDListBase[i].name);
   }
   free(rgb->vertexIDListBase);
   free(rgb->vertexBufferBase);
   free(rgb->vertexListBase);
   free(rgb->indexListBase);
   
   rgb->vertexIDListBase  = NULL;
   rgb->vertexBufferBase  = NULL;
   
   rgb->vertexListBase    = NULL;
   rgb->vertexListSize    = 0;
   rgb->vertexListCount   = 0;
   rgb->indexListBase     = NULL;
   rgb->indexListSize     = 0;
   rgb->indexListCount    = 0;
   rgb->vertexIDListCount = 0;

}

static inline struct rgbVertexID * rgbFindVertexIDFromName(const struct ryangeometrybuilder * rgb, const char * name)
{
   struct rgbVertexID * foundId = NULL;
   size_t i;
   for(i = 0; i < rgb->vertexIDListCount; i++)
   {
      if(strcmp(name, rgb->vertexIDListBase[i].name) == 0)
      {
         foundId = &rgb->vertexIDListBase[i];
         break;
      }
   }
   return foundId;
}

bool rgbSetVertexValue(struct ryangeometrybuilder * rgb, const char * name,  ...)
{
   struct rgbVertexID * id = rgbFindVertexIDFromName(rgb, name);
   va_list args;
   double * buffer;
   size_t i;
   
   if(id == NULL)
   {
      rlError("Failed to find data for %s", name);
      return false;
   }
   
   buffer = &rgb->vertexBufferBase[id->startsAt];
   
   va_start(args, name);
   
   for(i = 0; i < id->size; i++)
   {
         buffer[i] = va_arg(args, double);
   }
   
   va_end(args);
   
   return true;
}
static inline unsigned int rgbLocalAddVertex(struct ryangeometrybuilder * rgb)
{
   size_t vertexIndex;
   size_t offsetIndex;
   if(rgb->vertexListCount >= rgb->vertexListSize)
   {
      rgb->vertexListSize = rgb->vertexListCount + DEFAULT_GROWBY;
      rgb->vertexListBase = realloc(rgb->vertexListBase, sizeof(double) * rgb->vertexListSize * rgb->vertexSize);
   }
   vertexIndex = rgb->vertexListCount;
   offsetIndex = vertexIndex * rgb->vertexSize;
   memcpy(&rgb->vertexListBase[offsetIndex], 
          rgb->vertexBufferBase, 
          sizeof(double) * rgb->vertexSize);
   rgb->vertexListCount++;
   
   rgbClearVertexBuffer(rgb);
   
   return (unsigned int)vertexIndex;
}

unsigned int rgbAddVertex(struct ryangeometrybuilder * rgb)
{
   return rgbLocalAddVertex(rgb);
}

static inline void rgbLocalAddIndex(struct ryangeometrybuilder * rgb, unsigned int index)
{
   if(rgb->indexListCount >= rgb->indexListSize)
   {
      rgb->indexListSize = rgb->indexListCount + DEFAULT_GROWBY;
      rgb->indexListBase = realloc(rgb->indexListBase, sizeof(unsigned int) * rgb->indexListSize);
   }
   rgb->indexListBase[rgb->indexListCount] = index;
   rgb->indexListCount ++;
}

void rgbAddIndex(struct ryangeometrybuilder * rgb, unsigned int index)
{
   rgbLocalAddIndex(rgb, index);
}

unsigned int rgbAddVertexAndIndex(struct ryangeometrybuilder * rgb)
{
   unsigned int index;
   index = rgbLocalAddVertex(rgb);
   rgbLocalAddIndex(rgb, index);
   return index;
}

bool rgbSetProgramAttribute(struct ryangeometrybuilder * rgb, 
                            const char * name, struct rrattribute * rra)
{
   struct rgbVertexID * id = rgbFindVertexIDFromName(rgb, name);
   
   if(id == NULL)
   {
      rlError("Failed to find data for %s", name);
      return false;
   }
   
   id->rra = rra;
   return true;
}

union allpointertypes
{
   void     * v;
   uint8_t  * ui8;
   int8_t   * si8;
   uint16_t * ui16;
   int16_t  * si16;
   uint32_t * ui32;
   int32_t  * si32;
   float    * f;
};

static void * rgbConvertVertexDataTo(const struct ryangeometrybuilder * rgb, enum rrdatatype type)
{
   union allpointertypes data;
   const size_t elementCount = rgb->vertexListCount * rgb->vertexSize;
   const double * source = rgb->vertexListBase;
   size_t i;
   
#define CREATE_AND_CONVERT(unionType, elementSize)         \
   data.unionType = malloc((elementSize) * elementCount);  \
   for(i = 0; i < elementCount; i++)                       \
   {                                                       \
      data.unionType[i] = source[i];                       \
   }

   
   switch(type)
   {
   case eRRDT_UInt8:
      CREATE_AND_CONVERT(ui8, sizeof(uint8_t))
      break;
   case eRRDT_SInt8:
      CREATE_AND_CONVERT(si8, sizeof(int8_t))   
      break;
   case eRRDT_UInt16:
      CREATE_AND_CONVERT(ui16, sizeof(uint16_t))
      break;
   case eRRDT_SInt16:
      CREATE_AND_CONVERT(si16, sizeof(int16_t))
      break;
   case eRRDT_UInt32:
      CREATE_AND_CONVERT(ui32, sizeof(uint32_t))
      break;
   case eRRDT_SInt32:
      CREATE_AND_CONVERT(si32, sizeof(int32_t))
      break;
   case eRRDT_Float32:
      CREATE_AND_CONVERT(f, sizeof(float))
      break;
   default:
      data.v = NULL;
      break;
   }
#undef CONVERT_LOOP
   return data.v;
}

static struct rrvertexbuffer * rgbCreateRRVertexBuffer(struct ryangeometrybuilder * rgb,
                                                       struct ryanrenderer * rr,
                                                       enum rrdatatype type)
{
   void * data = rgbConvertVertexDataTo(rgb, type);
   const size_t elementCount = rgb->vertexListCount * rgb->vertexSize;
   struct rrvertexbuffer * rrvb = rrCreateVertexBuffer(rr, type, data, 
                                                       elementCount, 
                                                       eRRBUH_Never);

   free(data);
   return rrvb;
}

static void * rgbConvertIndexDataTo(const struct ryangeometrybuilder * rgb, enum rrdatatype type)
{
   union allpointertypes data;
   const size_t elementCount = rgb->indexListCount;
   const unsigned int * source = rgb->indexListBase;
   size_t i;
   
#define CREATE_AND_CONVERT(unionType, elementSize)         \
   data.unionType = malloc((elementSize) * elementCount);  \
   for(i = 0; i < elementCount; i++)                       \
   {                                                       \
      data.unionType[i] = source[i];                       \
   }

   
   switch(type)
   {
   case eRRDT_UInt8:
      CREATE_AND_CONVERT(ui8, sizeof(uint8_t))
      break;
   case eRRDT_SInt8:
      CREATE_AND_CONVERT(si8, sizeof(int8_t))   
      break;
   case eRRDT_UInt16:
      CREATE_AND_CONVERT(ui16, sizeof(uint16_t))
      break;
   case eRRDT_SInt16:
      CREATE_AND_CONVERT(si16, sizeof(int16_t))
      break;
   case eRRDT_UInt32:
      CREATE_AND_CONVERT(ui32, sizeof(uint32_t))
      break;
   case eRRDT_SInt32:
      CREATE_AND_CONVERT(si32, sizeof(int32_t))
      break;
   case eRRDT_Float32:
      CREATE_AND_CONVERT(f, sizeof(float))
      break;
   default:
      data.v = NULL;
      break;
   }
#undef CONVERT_LOOP
   return data.v;
}


static struct rrindexbuffer * rgbCreateRRIndexBuffer(struct ryangeometrybuilder * rgb,
                                                     struct ryanrenderer * rr,
                                                     enum rrdatatype type)
{
   void * data = rgbConvertIndexDataTo(rgb, type);
   struct rrindexbuffer * rrib = rrCreateIndexBuffer(rr, type, data, 
                                                     rgb->indexListCount, 
                                                     eRRBUH_Never);

   free(data);
   return rrib;
}

struct rrgeometry * rgbCreateRenderableGeometry(struct ryangeometrybuilder * rgb,
                                                struct ryanrenderer * rr,
                                                enum rrdatatype vertexType,
                                                enum rrdatatype indexType,
                                                enum rrdrawmode drawMode)
{
   size_t i;
   struct rrvertexbuffer * rrvb = rgbCreateRRVertexBuffer(rgb, rr, vertexType);
   struct rrindexbuffer * rrib = rgbCreateRRIndexBuffer(rgb, rr, indexType);
   struct rrgeometry * rrg = rrCreateGeometry(rr, rrvb, rgb->vertexSize, rrib, 
                                              drawMode);
   for(i = 0; i < rgb->vertexIDListCount; i++)
   {
      const struct rgbVertexID * id = &rgb->vertexIDListBase[i];
      if(id->rra != NULL)
      {
         rrgAddBufferData(rrg, id->rra, id->size, id->startsAt);
      }
   }

   return rrg;
}

void rgbClearData(struct ryangeometrybuilder * rgb)
{
   rgb->vertexListCount = 0;
   rgb->indexListCount  = 0;
   rgbClearVertexBuffer(rgb);
   rlDebug("Clearing Buffer Data");
}


