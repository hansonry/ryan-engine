#include <stdbool.h>
#include <stdlib.h>
#include <reStateTree.h>
#include <RyanLog.h>

enum reStateTreeNodeState
{
   eSTNS_unloaded,
   eSTNS_loadedDissabled,
   eSTNS_loadedEnabled, 
};

struct reStateTreeNode
{
   struct reStateTreeNodeData data;
   enum reStateTreeNodeState currentState;
   enum reStateTreeNodeState targetedState;
   struct reStateTreeNode * firstChild;
   struct reStateTreeNode * nextSibling;
};


struct reStateTree
{
   struct reStateTreeNode * rootNode;
};

static struct reStateTreeNode * reStateTreeNode_create(void * object,
                                                       reStateTree_callbackCreate callback)
{
   struct reStateTreeNode * node;
   node = malloc(sizeof(struct reStateTreeNode));
   node->data.object    = object;
   node->data.cbLoad    = NULL;
   node->data.cbUnload  = NULL;
   node->data.cbEnable  = NULL;
   node->data.cbDisable = NULL;
   node->data.cbEvent   = NULL;
   node->data.cbUpdate  = NULL;
   node->data.cbRender  = NULL;
   node->firstChild     = NULL;
   node->nextSibling    = NULL;
   node->currentState   = eSTNS_unloaded;
   node->targetedState  = eSTNS_loadedEnabled;
   
   
   if(callback != NULL)
   {
      callback(node, &node->data);
   }
   rlInfo("Created Tree Node 0x%p with object 0x%p", node, node->data.object);
   return node;
}


struct reStateTree * reStateTree_create(reStateTree_callbackCreate rootNodeCallback)
{
   struct reStateTree * tree;
   tree = malloc(sizeof(struct reStateTree));
   rlInfo("Created Tree 0x%p", tree);
   tree->rootNode = reStateTreeNode_create(NULL, rootNodeCallback);
   return tree;
}


static void reStateTreeNode_manageState(struct reStateTreeNode * node, 
                                        enum reStateTreeNodeState maxState);


static void reStateTreeNode_destroy(struct reStateTreeNode * node)
{
   struct reStateTreeNode * child, * tempChild;
   
   child = node->firstChild;
   while(child != NULL)
   {
      tempChild = child;
      child = child->nextSibling;
      reStateTreeNode_destroy(tempChild);
   }
   free(node);
}

void reStateTree_destroy(struct reStateTree * tree)
{
   
   reStateTreeNode_manageState(tree->rootNode, eSTNS_unloaded);
   reStateTreeNode_destroy(tree->rootNode);
   rlInfo("Destroy Tree 0x%p", tree);
   free(tree);
}

struct reStateTreeNode * reStateTree_getRootNode(struct reStateTree * tree)
{
   return tree->rootNode;
}


static void reStateTreeNode_runLoad(struct reStateTreeNode * node)
{
   struct reStateTreeNode * child;
   child = node->firstChild;
   while(child != NULL)
   {
      reStateTreeNode_runLoad(child);
      child = child->nextSibling;
   }
   
   if(node->data.cbLoad != NULL)
   {
      node->data.cbLoad(node->data.object);
   }
}

static void reStateTreeNode_runUnload(struct reStateTreeNode * node)
{
   struct reStateTreeNode * child;
   child = node->firstChild;
   while(child != NULL)
   {
      reStateTreeNode_runUnload(child);
      child = child->nextSibling;
   }
   
   if(node->data.cbUnload != NULL)
   {
      node->data.cbUnload(node->data.object);
   }
}

static void reStateTreeNode_runEvent(struct reStateTreeNode * node, SDL_Event * event)
{
   struct reStateTreeNode * child;
   if(node->currentState == eSTNS_loadedEnabled)
   {
      child = node->firstChild;
      while(child != NULL)
      {
         reStateTreeNode_runEvent(child, event);
         child = child->nextSibling;
      }
      if(node->data.cbEvent != NULL)
      {
         node->data.cbEvent(node->data.object, event);
      }
   }
}

static void reStateTreeNode_runUpdate(struct reStateTreeNode * node, float seconds)
{
   struct reStateTreeNode * child;
   if(node->currentState == eSTNS_loadedEnabled)
   {
      child = node->firstChild;
      while(child != NULL)
      {
         reStateTreeNode_runUpdate(child, seconds);
         child = child->nextSibling;
      }
      if(node->data.cbUpdate != NULL)
      {
         node->data.cbUpdate(node->data.object, seconds);
      }
   }
}

static void reStateTreeNode_runRender(struct reStateTreeNode * node)
{
   struct reStateTreeNode * child;
   if(node->currentState == eSTNS_loadedEnabled)
   {
      child = node->firstChild;
      while(child != NULL)
      {
         reStateTreeNode_runRender(child);
         child = child->nextSibling;
      }
      if(node->data.cbRender != NULL)
      {
         node->data.cbRender(node->data.object);
      }
   }
}

static void reStateTreeNode_manageState(struct reStateTreeNode * node, 
                                        enum reStateTreeNodeState maxState)
{
   struct reStateTreeNode * child;
   enum reStateTreeNodeState nextState;
   
   if(maxState < node->targetedState)
   {
      nextState = maxState;
   }
   else
   {
      nextState = node->targetedState;
   }
   
   child = node->firstChild;
   while(child != NULL)
   {
      reStateTreeNode_manageState(child, nextState);
      child = child->nextSibling;
   }

   while(node->currentState != nextState)
   {
      if(node->currentState > nextState)
      {
         switch(node->currentState)
         {
         case eSTNS_loadedEnabled:
            if(node->data.cbDisable != NULL)
            {
               node->data.cbDisable(node->data.object);
            }
            node->currentState = eSTNS_loadedDissabled;
            break;
         case eSTNS_loadedDissabled:
            if(node->data.cbUnload != NULL)
            {
               node->data.cbUnload(node);
            }            
            node->currentState = eSTNS_unloaded;
            break;
         default:
            rlWarning("Impossible Decreasing State");
            break;
         }
      }
      else
      {
         switch(node->currentState)
         {
         case eSTNS_unloaded:
            if(node->data.cbLoad != NULL)
            {
               node->data.cbLoad(node->data.object);
            }
            node->currentState = eSTNS_loadedDissabled;
            break;
         case eSTNS_loadedDissabled:
            if(node->data.cbEnable != NULL)
            {
               node->data.cbEnable(node->data.object);
            }
            node->currentState = eSTNS_loadedEnabled;
            break;
         default:
            rlWarning("Impossible Increasing State");
            break;
         }
      }
   }

}

void reStateTree_runLoad(struct reStateTree * tree)
{
   reStateTreeNode_runLoad(tree->rootNode);
}

void reStateTree_runUnload(struct reStateTree * tree)
{
   reStateTreeNode_runUnload(tree->rootNode);
}

void reStateTree_runEvent(struct reStateTree * tree, SDL_Event * event)
{
   rlTrace("Entering Tree Event Callback, Type: %d", event->type);
   reStateTreeNode_runEvent(tree->rootNode, event);
   rlTrace("Exiting Tree Event Callback, Type: %d", event->type);
}

void reStateTree_runUpdate(struct reStateTree * tree, float seconds)
{
   rlTrace("Entering Tree Update. dt: %f", seconds);
   reStateTreeNode_runUpdate(tree->rootNode, seconds);
   rlTrace("Exiting Tree Update. dt: %f", seconds);
}

void reStateTree_runRender(struct reStateTree * tree)
{
   rlTrace("Entering Tree Render");
   reStateTreeNode_runRender(tree->rootNode);
   rlTrace("Exiting Tree Render");
}

void reStateTree_manageState(struct reStateTree * tree)
{
   reStateTreeNode_manageState(tree->rootNode, eSTNS_loadedEnabled);
}

void reStateTree_enableNode(struct reStateTreeNode * node)
{
   node->targetedState = eSTNS_loadedEnabled;
}

void reStateTree_disableNode(struct reStateTreeNode * node)
{
   node->targetedState = eSTNS_loadedDissabled;
}


static void reStateTreeNode_addToLastChild(struct reStateTreeNode * parent, 
                                           struct reStateTreeNode * newChild)
{
   struct reStateTreeNode * child;
   
   if(parent->firstChild == NULL)
   {
      parent->firstChild = newChild;
   }
   else
   {
      // Find the last child of the sibling list
      child = parent->firstChild;
      while(child->nextSibling != NULL)
      {
         child = child->nextSibling;
      }
      
      // Append the new child
      child->nextSibling = newChild;
   }
   
}

struct reStateTreeNode * reStateTree_addNode(struct reStateTreeNode * parent, 
                                             void * object, 
                                             reStateTree_callbackCreate callback)
{
   struct reStateTreeNode * newChild;
   newChild = reStateTreeNode_create(object, callback);
   reStateTreeNode_addToLastChild(parent, newChild);
   return newChild;
}

