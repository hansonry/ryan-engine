#include <reCameraOrbit.h>
#include <reTForm.h>
#include <RyanLog.h>

static void reCameraOrbit_node(struct reStateTreeNode * node, 
                               struct reStateTreeNodeData * data)
{
   if(data->object == NULL)
   {
      rlError("Expected object to contain Camera");
   }
   else
   {
      data->cbEvent  = (reStateTree_callbackEvent)  reCameraOrbit_event;
      data->cbUpdate = (reStateTree_callbackUpdate) reCameraOrbit_update;
   }
}

struct reStateTreeNode * reCameraOrbit_addNode(struct reCameraOrbit * cam, 
                                               struct reStateTreeNode * node)
{
   return reStateTree_addNode(node, cam, reCameraOrbit_node);
}

void reCameraOrbit_setAll(struct reCameraOrbit * cam, 
                          float ox, float oy, float oz, 
                          float distance, float yaw, float pitch)
{
   (void)reVec3f_set(&cam->origin, ox, oy, oz);
   cam->distance       = distance;
   cam->targetDistance = distance;
   cam->yaw            = yaw;
   cam->pitch          = pitch;
}

void reCameraOrbit_setOrigin(struct reCameraOrbit * cam,
                             float ox, float oy, float oz)
{
   (void)reVec3f_set(&cam->origin, ox, oy, oz);
}

static float slew(float current, float target, float max)
{
   if(current > target)
   {
      if((current - target) < max)
      {
         return target;
      }
      else
      {
         return current - max;
      }
   }
   else if(current < target)
   {
      if((target - current) < max)
      {
         return target;
      }
      else
      {
         return current + max;
      }
   }

   return target;
}

void reCameraOrbit_update(struct reCameraOrbit * cam, 
                          float seconds)
{
   cam->distance = slew(cam->distance, cam->targetDistance, 4 * seconds);
}


#define MAX_PITCH (3.14 / 2)
void reCameraOrbit_event(struct reCameraOrbit * cam,
                         SDL_Event * event)
{
   if(event->type == SDL_MOUSEWHEEL)
   {
      cam->targetDistance -= event->wheel.y * 0.5;
      if(cam->targetDistance < 1)
      {
         cam->targetDistance = 1;
      }
   }
   if(event->type == SDL_MOUSEMOTION &&
      (event->motion.state & SDL_BUTTON_RMASK) == SDL_BUTTON_RMASK)
   {
      cam->yaw   += event->motion.xrel * 1 * 3.14 / 180;
      cam->pitch += event->motion.yrel * 1 * 3.14 / 180;

      // limmit the pitch to top and bottom
      if(cam->pitch > MAX_PITCH)
      {
         cam->pitch = MAX_PITCH;
      }
      else if(cam->pitch < -MAX_PITCH)
      {
         cam->pitch = -MAX_PITCH;
      }
      
   }
}

float * reCameraOrbit_computeMatrix(const struct reCameraOrbit * cam, 
                                    float * mat)
{
   reTForm_setIdentity(mat);
   reTForm_translateZ(mat, cam->distance);
   reTForm_rotateX(mat, -cam->pitch);
   reTForm_rotateY(mat, -cam->yaw);
   return mat;
}

