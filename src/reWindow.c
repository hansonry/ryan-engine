#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <GL/gl.h>
#include <reWindow.h>
#include <RyanLog.h>
#include <reSys.h>
#include <reStateTree.h>

static volatile bool Running;
static volatile bool CloseOnXorESC = true;
static reWindow_callbackCreated CreatedCallback = NULL;

static SDL_Window * Window = NULL;

void reWindow_setCreatedCallback(reWindow_callbackCreated callback)
{
   CreatedCallback = callback;
   rlInfo("Setting Created callback to 0x%p", callback);
}

void reWindow_setCloseOnXorESC(int flag)
{
   if(flag)
   {
      CloseOnXorESC = true;
   }
   else
   {
      CloseOnXorESC = false;
   }
   rlInfo("Setting CloseOnXorESC to %d", CloseOnXorESC);
}

void reWindow_stop(void)
{
   Running = false;
   rlInfo("Exiting due to application request");
}

SDL_Window * reWindow_getSDLHandle(void)
{
   return Window;
}

void reWindow_getSize(int * width, int * height)
{
   if(Window != NULL)
   {
      SDL_GetWindowSize(Window, width, height);
   }
}

int reWindow_runDefaults(reStateTree_callbackCreate applicationCallback)
{
   return reWindow_run("Ryan Engine", 
                       SDL_WINDOWPOS_CENTERED, 
                       SDL_WINDOWPOS_CENTERED, 
                       620, 
                       387, 
                       0,
                       applicationCallback);
}

int reWindow_run(const char * title, int x, int y, int w, int h, Uint32 flags,
                 reStateTree_callbackCreate applicationCallback)
{
   GLenum glewResult;
   SDL_GLContext * glContext;
   SDL_Event event;
   Uint32 prevUpdateTicks;
   Uint32 tempTicks;
   GLenum errorCode;
   float updateTime;
   struct reStateTree * tree;


   if(reSys_setup())
   {
      return 1;
   }

   Window = SDL_CreateWindow(title, 
                             x, y, 
                             w, h, 
                             SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | flags);
   if(Window == NULL) 
   {
      rlCritical("SDL_CreateWindow Error: %s", SDL_GetError());
      SDL_Quit();
      rlCleanup();
      return 1;
   }
   rlInfo("SDL Window Created");
   
   if(CreatedCallback != NULL)
   {
      CreatedCallback(Window, w, h);
   }

   glContext = SDL_GL_CreateContext(Window);
   if(glContext == NULL) 
   {
      rlCritical("SDL_GL_CreateContext Error: %s", SDL_GetError());
      SDL_DestroyWindow(Window);
      SDL_Quit();
      rlCleanup();
      return 1;
   }
   rlInfo("SDL OpenGL Context Created");
   
   glewResult = glewInit();
   if(glewResult != GLEW_OK)
   {
      rlCritical("glewInit() Error: %s", glewGetErrorString(glewResult));
      SDL_GL_DeleteContext(glContext);
      SDL_DestroyWindow(Window);
      SDL_Quit();
      rlCleanup();
      return 1;
   }
   rlInfo("GLEW Initalized");

   // GL Defaults
   glViewport(0, 0, w, h);
   glClearColor(0.0f, 0.0f, 0.15f, 1.0f);
   rlInfo("OpenGL Inital State Setup Completed");
   tree = reStateTree_create(applicationCallback);
   // Give time to transition state to loadedEnabled.
   reStateTree_manageState(tree);
   // Check if there is an opengl error state and close if there is a problem
   if((errorCode = glGetError()) != GL_NO_ERROR)
   {
      rlError("OpenGL Error detected after load. Reason: %s", gluErrorString(errorCode));
      Running = false;
   }
   else
   {
      Running = true;
   }
   prevUpdateTicks = SDL_GetTicks();
   while(Running)
   {
      reStateTree_manageState(tree);
      while(SDL_PollEvent(&event))
      {
         reStateTree_runEvent(tree, &event);
         
         if(CloseOnXorESC && 
            (event.type == SDL_QUIT || 
             (event.type == SDL_KEYDOWN &&
              event.key.keysym.sym == SDLK_ESCAPE)))
         {
            rlInfo("Exiting due to CloseOnXorESC flag");
            Running = false;
         }
      }

      tempTicks = SDL_GetTicks();
      updateTime = (tempTicks - prevUpdateTicks) / 1000.0f;
      reStateTree_runUpdate(tree, updateTime);
      prevUpdateTicks = tempTicks;

      reStateTree_runRender(tree);

      SDL_GL_SwapWindow(Window);
   }
   rlInfo("Exiting Main Loop");
   reStateTree_manageState(tree);

   reStateTree_destroy(tree);
   SDL_GL_DeleteContext(glContext);
   rlInfo("SDL GL Context Deleted");
   SDL_DestroyWindow(Window);
   Window = NULL;
   rlInfo("SDL Window Detroyed");
   rlInfo("Full Shutdown Sucessful");
   reSys_teardown();
   return 0;
}

