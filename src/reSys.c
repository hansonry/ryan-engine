#include <reSys.h>
#include <RyanLog.h>
#include <SDL.h>

static int Setup = 0;

int reSys_setup()
{
   if(!Setup)
   {
      rlInit();
      rlInfo("Logging Initalized");
      if (SDL_Init(SDL_INIT_EVERYTHING) != 0) 
      {
         rlCritical("SDL_Init Error: %s\n", SDL_GetError());
         rlCleanup();
         return 1;
      }
      rlInfo("SDL Initalized");
      Setup = 1;
   }
   return 0;
}

void reSys_teardown()
{
   Setup = 0;
   SDL_Quit();
   rlInfo("SDL Quit");
   rlCleanup();
}


