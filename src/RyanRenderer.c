
#include <RyanRenderer.h>
#include <RyanIO.h>

struct rrprogram * rrCreateProgramFromFiles(struct ryanrenderer * rr,
                                            const char * vertexFilename,
                                            const char * geometryFilename,
                                            const char * fragmentFilename)
{
   struct ryanbyteinputstream * vertexStream, 
                              * geometryStream, 
                              * fragmentStream;
   struct rrprogram * program;

   if(vertexFilename != NULL)
   {
      vertexStream = rbisCreateFromFile(vertexFilename);
   }
   else
   {
      vertexStream = NULL;
   }
   if(geometryFilename != NULL)
   {
      geometryStream = rbisCreateFromFile(geometryFilename);
   }
   else
   {
      geometryStream = NULL;
   }
   if(fragmentFilename != NULL)
   {
      fragmentStream = rbisCreateFromFile(fragmentFilename);
   }
   else
   {
      fragmentStream = NULL;
   }

   program = rrCreateProgramFromStreams(rr, vertexStream, 
                                            geometryStream, 
                                            fragmentStream);
   if(vertexStream != NULL)
   {
      rbisDestroy(vertexStream);
   }
   if(geometryStream != NULL)
   {
      rbisDestroy(geometryStream);
   }
   if(fragmentStream != NULL)
   {
      rbisDestroy(fragmentStream);
   }
   return program;
}

struct rrprogram * rrCreateProgramFromStrings(struct ryanrenderer * rr,
                                              const char * vertexCode,
                                              const char * geometryCode,
                                              const char * fragmentCode)
{
   struct ryanbyteinputstream * vertexStream, 
                              * geometryStream, 
                              * fragmentStream;
   struct rrprogram * program;

   if(vertexCode != NULL)
   {
      vertexStream = rbisCreateFromString(vertexCode);
   }
   else
   {
      vertexStream = NULL;
   }
   if(geometryCode != NULL)
   {
      geometryStream = rbisCreateFromString(geometryCode);
   }
   else
   {
      geometryStream = NULL;
   }
   if(fragmentCode != NULL)
   {
      fragmentStream = rbisCreateFromString(fragmentCode);
   }
   else
   {
      fragmentStream = NULL;
   }

   program = rrCreateProgramFromStreams(rr, vertexStream, 
                                            geometryStream, 
                                            fragmentStream);
   if(vertexStream != NULL)
   {
      rbisDestroy(vertexStream);
   }
   if(geometryStream != NULL)
   {
      rbisDestroy(geometryStream);
   }
   if(fragmentStream != NULL)
   {
      rbisDestroy(fragmentStream);
   }
   return program;
}


