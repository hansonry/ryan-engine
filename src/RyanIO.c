#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <RyanLog.h>
#include <RyanIO.h>


struct bufferbyteinputstream
{
   struct ryanbyteinputstream parent;
   void * buffer;
   size_t size;
   size_t index;
   bool owned;
};

#define TYPE_SET(ct, name, src) ct name = (ct )src

static void bbisDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct bufferbyteinputstream *, bbis, ro);
   if(bbis->owned)
   {
      free(bbis->buffer);
   }
   free(bbis);
}

static size_t bbisRead(struct ryanbyteinputstream * rbis, void * buffer, size_t bufferSize)
{
   TYPE_SET(struct bufferbyteinputstream *, bbis, rbis);
   const size_t spaceLeft = bbis->size - bbis->index;
   size_t toRead;
   
   if(bufferSize < spaceLeft)
   {
      toRead = bufferSize;
   }
   else
   {
      toRead = spaceLeft;
   }

   memcpy(buffer, ((uint8_t *)bbis->buffer) + bbis->index, toRead);
   bbis->index += toRead;
   return toRead;
}

static bool bbisIsEndOfStream(struct ryanbyteinputstream * rbis)
{
   TYPE_SET(struct bufferbyteinputstream *, bbis, rbis);
   return bbis->index >= bbis->size;
}



#define SET_FUNCTION(name) rbis->fp##name = bbis##name

static inline struct ryanbyteinputstream * rbisCreateFromBufferGeneral(void * buffer, size_t size, bool owned)
{
   struct bufferbyteinputstream * bbis;
   struct ryanbyteinputstream * rbis;
   
   if(buffer == NULL)
   {
      rlWarning("Passed In Buffer is NULL");
      return NULL;
   }

   bbis = malloc(sizeof(struct bufferbyteinputstream));
   rbis = &bbis->parent;

   rbis->parent.fpDestroy = bbisDestroy;
   SET_FUNCTION(Read);
   SET_FUNCTION(IsEndOfStream);

   bbis->buffer = buffer;
   bbis->size   = size;
   bbis->index  = 0;
   bbis->owned  = owned;

   rbis->id = "From Buffer";
   return rbis;
}

#undef SET_FUNCTION

struct ryanbyteinputstream * rbisCreateFromBuffer(const void * buffer, size_t size)
{
   // This should be funtionaly const
   // Forgive me const gods for what I have done
   return rbisCreateFromBufferGeneral((void*)buffer, size, false);
}

struct ryanbyteinputstream * rbisCreateFromBufferOwned(void * buffer, size_t size)
{
   return rbisCreateFromBufferGeneral(buffer, size, true);
}

struct ryanbyteinputstream * rbisCreateFromString(const char * string)
{
   return rbisCreateFromBuffer(string, strlen(string));
}

struct ryanbyteinputstream * rbisCreateFromStringOwned(char * string)
{
   return rbisCreateFromBufferOwned(string, strlen(string));
}


struct filebyteinputstream
{
   struct ryanbyteinputstream parent;
   FILE * fh;
   bool ownesFileHandle;
};


static void fbisDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct filebyteinputstream *, fbis, ro);
   if(fbis->ownesFileHandle)
   {
      fclose(fbis->fh);
   }
   free(fbis);
}

static size_t fbisRead(struct ryanbyteinputstream * rbis, void * buffer, size_t bufferSize)
{
   TYPE_SET(struct filebyteinputstream *, fbis, rbis);
   return fread(buffer, 1, bufferSize, fbis->fh);

}

static bool fbisIsEndOfStream(struct ryanbyteinputstream * rbis)
{
   TYPE_SET(struct filebyteinputstream *, fbis, rbis);
   return feof(fbis->fh) != 0;
}


#undef TYPE_SET

struct ryanbyteinputstream * rbisCreateFromFile(const char * filename)
{
   struct ryanbyteinputstream * rbis;
   struct filebyteinputstream * fbis;
   FILE * fh;

   if(filename == NULL)
   {
      rlError("Filename passed in was NULL");
      return NULL;
   }

   fh = fopen(filename, "rb");
   if(fh == NULL)
   {
      rlError("File failed to open for reading: %s", filename);
      return NULL;
   }

   rbis = rbisCreateFromFileHandle(fh);
   fbis = (struct filebyteinputstream *)rbis;
   fbis->ownesFileHandle = true;
   
   rbis->id = filename;
   return rbis;
}

#define SET_FUNCTION(name) rbis->fp##name = fbis##name

struct ryanbyteinputstream * rbisCreateFromFileHandle(FILE * fileHandle)
{
   struct filebyteinputstream * fbis;
   struct ryanbyteinputstream * rbis;
   
   if(fileHandle == NULL)
   {
      rlError("Passed in file handle was NULL");
      return NULL;
   }

   fbis = malloc(sizeof(struct filebyteinputstream));
   rbis = &fbis->parent;
  
   rbis->parent.fpDestroy = fbisDestroy;
   SET_FUNCTION(Read);
   SET_FUNCTION(IsEndOfStream);

   fbis->fh = fileHandle;
   fbis->ownesFileHandle = false;
   rbis->id = "File Handle";
   return rbis;
}

#undef SET_FUNCTION



void * rbisDumpToMemory(struct ryanbyteinputstream * rbis, size_t * readSize, size_t growby)
{
   uint8_t * data = NULL;
   size_t size    = 0;
   size_t index   = 0;

   if(growby == 0)
   {
      growby = 512;
   }

   while(!rbisIsEndOfStream(rbis))
   {
      if(index >= size)
      {
         size = index + growby;
         data = realloc(data, size);
      }

      index += rbisRead(rbis, data + index, size - index);
   }

   if(readSize != NULL)
   {
      *readSize = index;
   }
   return data;
}

