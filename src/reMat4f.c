#include <stdarg.h>
#include <reMat4f.h>



float * reMat4f_multiplyAll(float * dest, int count, ...)
{
   int i;
   va_list ap;
   const float * matP;
   reMat4f_identity(dest);
   va_start(ap, count);
   for(i = 0; i < count; i++)
   {
      matP = va_arg(ap, const float * );
      reMat4f_mulitply(dest, dest, matP);
   }
   va_end(ap);
   return dest;
}

