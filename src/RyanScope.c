#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <RyanScope.h>

#ifndef RYANSCOPE_CHUNK_SIZE
#define RYANSCOPE_CHUNK_SIZE 32
#endif // RYANSCOPE_CHUNK_SIZE


// using the address
static int rsMarker = 0;

typedef void (*rsfpFree)(void * obj);

void rsInit(struct ryanscope * rs)
{
   rs->base  = NULL;
   rs->size  = 0;
   rs->count = 0 ;
}

void rsCleanup(struct ryanscope * rs)
{

   unsigned int i = rs->count - 2;
   while(i < rs->count)
   {
      void * obj = rs->base[i];
      i--;
      while(i < rs->count && 
            rs->base[i] != &rsMarker)
      {
         rsfpFree fpFree = rs->base[i];
         fpFree(obj);
         i--;
      }
      i--;
   }
   

   if(rs->base != NULL)
   {
      free(rs->base);
   }
   rs->base  = NULL;
   rs->size  = 0;
   rs->count = 0;
}

static inline void rsGrowIfNessary(struct ryanscope * rs)
{
   if(rs->count >= rs->size)
   {
      rs->size = rs->count +  RYANSCOPE_CHUNK_SIZE;
      rs->base = realloc(rs->base, rs->size * sizeof(void*));
   }
}

static inline void rsWritePointer(struct ryanscope * rs, void * pointer)
{
   rsGrowIfNessary(rs);

   rs->base[rs->count] = pointer;
   rs->count ++;
}

static inline void rsSwap(struct ryanscope * rs, unsigned int indexA, unsigned int indexB)
{
   void * tmp;
   tmp = rs->base[indexA];
   rs->base[indexA] = rs->base[indexB];
   rs->base[indexB] = tmp;
}

static inline void rsReverse(struct ryanscope * rs, unsigned int start, unsigned int end)
{
   while(start < end)
   {
      rsSwap(rs, start, end);
      start ++;
      end --;
   }
}

void rsAddUntilNull(struct ryanscope * rs, void * data, ...)
{
   va_list args;
   rsfpFree fpFree;
   unsigned int startIndex = rs->count;
   
   va_start(args, data);

   while((fpFree = va_arg(args, rsfpFree)) != NULL)
   {
      rsWritePointer(rs, fpFree);
   }
   va_end(args);

   rsReverse(rs, startIndex, rs->count - 1);

   rsWritePointer(rs, data);
   rsWritePointer(rs, &rsMarker);

}

#include <RyanObject.h>

void rsAddObj(struct ryanscope * rs, void * data)
{
   rsAddUntilNull(rs, data, roDestroy, NULL);
}
