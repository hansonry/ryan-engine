#include <GL/glew.h>
#include <stdlib.h>
#include <math.h>
#include <reShape.h>

void reShape_teardown(struct reShape * shape)
{
   glDeleteVertexArrays(1, &shape->vertexArrayObject);
   glDeleteBuffers(1, &shape->arrayVertexBufferObject);
   glDeleteBuffers(1, &shape->elementArrayVertexBufferObject);
   shape->vertexArrayObject              = 0;
   shape->arrayVertexBufferObject        = 0;
   shape->elementArrayVertexBufferObject = 0;
   shape->mode                           = 0;
   shape->count                          = 0;
}

static void reShape_createVAOVBO(struct reShape * shape, int includesTextureCoords,
                                 float * vertexData, GLsizeiptr vertexSize, 
                                 unsigned short * elementData, GLsizeiptr elementSize)
{
   GLsizei stride;
   glGenVertexArrays(1, &shape->vertexArrayObject);
   glBindVertexArray(shape->vertexArrayObject);

   glGenBuffers(1, &shape->arrayVertexBufferObject);
   glBindBuffer(GL_ARRAY_BUFFER, shape->arrayVertexBufferObject);
   glBufferData(GL_ARRAY_BUFFER, 
                vertexSize, 
                vertexData, 
                GL_STATIC_DRAW);


   glGenBuffers(1, &shape->elementArrayVertexBufferObject);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape->elementArrayVertexBufferObject);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
                elementSize, 
                elementData, 
                GL_STATIC_DRAW);
                
   if(includesTextureCoords)
   {
      stride = sizeof(float) * 8;
   }
   else
   {
      stride = sizeof(float) * 6;
   }

   glEnableVertexAttribArray(0);
   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);
   glEnableVertexAttribArray(1);
   glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(float) * 3));
   if(includesTextureCoords)
   {
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(float) * 6)); 
   }      
   
   // Unbind everything
   glBindVertexArray(0);
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
   glDisableVertexAttribArray(0);
   glDisableVertexAttribArray(1);
}

// Shapes


static void reShape_GenerateBoxData(float * virtexData, 
                                    float xMin, float yMin, float zMin,
                                    float xMax, float yMax, float zMax)
{
   #define WRITE(x, y, z, nx, ny, nz) \
   virtexData[0] = x; virtexData[1] = y; virtexData[2] = z; \
   virtexData[3] = nx; virtexData[4] = ny; virtexData[5] = nz; virtexData += 6
   
   WRITE(xMin, yMax, zMax,  0.0f,  1.0f,  0.0f);
   WRITE(xMax, yMax, zMax,  0.0f,  1.0f,  0.0f);
   WRITE(xMax, yMax, zMin,  0.0f,  1.0f,  0.0f);
   WRITE(xMin, yMax, zMin,  0.0f,  1.0f,  0.0f);

   WRITE(xMax, yMax, zMin,  1.0f,  0.0f,  0.0f);
   WRITE(xMax, yMax, zMax,  1.0f,  0.0f,  0.0f);
   WRITE(xMax, yMin, zMax,  1.0f,  0.0f,  0.0f);
   WRITE(xMax, yMin, zMin,  1.0f,  0.0f,  0.0f);

   WRITE(xMin, yMax, zMax,  0.0f,  0.0f,  1.0f);
   WRITE(xMin, yMin, zMax,  0.0f,  0.0f,  1.0f);
   WRITE(xMax, yMin, zMax,  0.0f,  0.0f,  1.0f);
   WRITE(xMax, yMax, zMax,  0.0f,  0.0f,  1.0f);

   WRITE(xMin, yMin, zMin,  0.0f, -1.0f,  0.0f);
   WRITE(xMax, yMin, zMin,  0.0f, -1.0f,  0.0f);
   WRITE(xMax, yMin, zMax,  0.0f, -1.0f,  0.0f);
   WRITE(xMin, yMin, zMax,  0.0f, -1.0f,  0.0f);

   WRITE(xMin, yMax, zMin, -1.0f,  0.0f,  0.0f);
   WRITE(xMin, yMin, zMin, -1.0f,  0.0f,  0.0f);
   WRITE(xMin, yMin, zMax, -1.0f,  0.0f,  0.0f);
   WRITE(xMin, yMax, zMax, -1.0f,  0.0f,  0.0f);

   WRITE(xMax, yMin, zMin,  0.0f,  0.0f, -1.0f);
   WRITE(xMin, yMin, zMin,  0.0f,  0.0f, -1.0f);
   WRITE(xMin, yMax, zMin,  0.0f,  0.0f, -1.0f);
   WRITE(xMax, yMax, zMin,  0.0f,  0.0f, -1.0f);
   
   #undef WRITE
}

void reShape_box(struct reShape * shape, float x1, float y1, float z1, 
                                         float x2, float y2, float z2)
{
   float xMin, xMax, yMin, yMax, zMin, zMax;
   float cubeVirtexData[6 * 24];

#define FACES(n) n, n+2, n+1, n+2, n, n+3
   unsigned short cubeIndexData[] = 
   {
      FACES(0),
      FACES(4),
      FACES(8),
      FACES(12),
      FACES(16),
      FACES(20),
   };
#undef FACES

   if(x1 > x2)
   {
      xMin = x2;
      xMax = x1;
   }
   else
   {
      xMin = x1;
      xMax = x2;
   }
   
   if(y1 > y2)
   {
      yMin = y2;
      yMax = y1;
   }
   else
   {
      yMin = y1;
      yMax = y2;
   }
   
   if(z1 > z2)
   {
      zMin = z2;
      zMax = z1;
   }
   else
   {
      zMin = z1;
      zMax = z2;
   }
   reShape_GenerateBoxData(cubeVirtexData, xMin, yMin, zMin, xMax, yMax, zMax);
   
   
   reShape_createVAOVBO(shape, 0,
                        cubeVirtexData, sizeof(cubeVirtexData),
                        cubeIndexData,  sizeof(cubeIndexData));

   shape->mode  = GL_TRIANGLES;
   shape->count = 36; 
}

void reShape_cube(struct reShape * shape)
{
   reShape_box(shape, 0, 0, 0, 1, 1, 1);
}


void reShape_circle(struct reShape * shape, float xCenter, float yCenter, float radius, unsigned int points)
{
   reShape_pie(shape, xCenter, yCenter, radius, points, 0, 3.142 * 2);
}

void reShape_pie(struct reShape * shape, float xCenter, float yCenter, float radius, 
                 unsigned int points, float startRadians, float endRadians)
{
   unsigned int numberOfVertexs = points + 2;
   unsigned int i;
   float * a, angle;
   float delatRadians = (endRadians - startRadians) / points;
   unsigned short * pieIndexData = malloc(sizeof(float) * numberOfVertexs);
   float * pieVirtexData = malloc(sizeof(float) * numberOfVertexs * 6);
   
   #define WRITE(a, x, y) \
   a[0] = x; a[1] = y; a[2] = 0; a[3] = 0; a[4] = 0; a[5] = -1
   
   WRITE(pieVirtexData, xCenter, yCenter);
   a = pieVirtexData + 6;
   angle = startRadians;
   for(i = 0; i < (points + 1); i++)
   {
      float lx, ly;
      lx = cos(angle) * radius + xCenter;
      ly = sin(angle) * radius + yCenter;
      WRITE(a, lx, ly);
      a += 6;
      angle += delatRadians;
      
   }
   #undef WRITE
   
   for(i = 0; i < numberOfVertexs; i++)
   {
      pieIndexData[i] = i;
   }
   
   reShape_createVAOVBO(shape, 0,
                        pieVirtexData, sizeof(float) * numberOfVertexs * 6,
                        pieIndexData,  sizeof(float) * numberOfVertexs);

   shape->mode  = GL_TRIANGLE_FAN;
   shape->count = numberOfVertexs; 
   
   free(pieIndexData);
   free(pieVirtexData);
}

void reShape_rectangle(struct reShape * shape, float x1, float y1, 
                                               float x2, float y2)
{
   float xMin, yMin, xMax, yMax;
   unsigned short rectangleIndexData[4];
   float rectangleVirtexData[8 * 4];
   float * a;
   if(x1 > x2)
   {
      xMin = x2;
      xMax = x1;
   }
   else
   {
      xMin = x1;
      xMax = x2;
   }
   
   if(y1 > y2)
   {
      yMin = y2;
      yMax = y1;
   }
   else
   {
      yMin = y1;
      yMax = y2;
   }
   
   #define WRITE(a, x, y, u, v) \
   a[0] = x; a[1] = y; a[2] = 0; \
   a[3] = 0; a[4] = 0; a[5] = -1; \
   a[6] = u; a[7] = v; a += 8
   
   a = rectangleVirtexData;
   WRITE(a, xMax, yMax, 1, 1);
   WRITE(a, xMin, yMax, 0, 1);
   WRITE(a, xMax, yMin, 1, 0);
   WRITE(a, xMin, yMin, 0, 0);
   
   #undef WRITE
   
   rectangleIndexData[0] = 0;
   rectangleIndexData[1] = 1;
   rectangleIndexData[2] = 2;
   rectangleIndexData[3] = 3;
   
   reShape_createVAOVBO(shape, 1,
                        rectangleVirtexData, sizeof(float) * 4 * 8,
                        rectangleIndexData,  sizeof(float) * 4);   
   
   shape->mode  = GL_TRIANGLE_STRIP;
   shape->count = 4; 

}

/*
void reShape_cylinder(struct reShape * shape, float height, 
                                              float topRadius, 
                                              float bottomRadius,
                                              unsigned int sides)
{
   unsigned short vertexCount;
   unsigned int elementCount;
   unsigned short * cylinderIndexData;
   float * cylinderVirtexData;
   float * a;
   int bottomPointFlag;
   int topPointFlag;
   
   // Compute Points
   if(topRadius == 0)
   {
      topPointFlag = 1;
      if(bottomRadius == 0)
      {
         bottomRadius = 1;
      }
   }
   else
   {
      topPointFlag = 0;
   }
   
   if(bottomRadius == 0)
   {
      bottomPointFlag = 1;      
   }
   else
   {
      bottomPointFlag = 0;
   }
   
   if(bottomPointFlag || topPointFlag)
   {
      vertexCount = sides * 4 + 1;
      elementCount = sides * 6;
   }
   else
   {
      vertexCount = sides * 6 + 2;
      elementCount = sides * 12;
   }
   
   cylinderIndexData  = malloc(sizeof(float) * elementCount);
   cylinderVirtexData = malloc(sizeof(float) * vertexCount);
   
   
   
   #define WRITE(a, x, y) \
   a[0] = x; a[1] = y; a[2] = 0; \
   a[3] = 0; a[4] = 0; a[5] = -1; a += 6
   
   a = rectangleVirtexData;
   WRITE(a, xMax, yMax);
   WRITE(a, xMin, yMax);
   WRITE(a, xMax, yMin);
   WRITE(a, xMin, yMin);
   
   #undef WRITE
   
   rectangleIndexData[0] = 0;
   rectangleIndexData[1] = 1;
   rectangleIndexData[2] = 2;
   rectangleIndexData[3] = 3;
   
   reShape_createVAOVBO(shape, cylinderVirtexData, sizeof(float) * vertexCount,
                               cylinderIndexData,  sizeof(float) * elementCount);   
   
   shape->mode  = GL_TRIANGLES;
   shape->count = elementCount; 


   free(cylinderIndexData);
   free(cylinderVirtexData);
}
*/
