#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <reShader.h>
#include <RyanLog.h>

static const char * reShader_getShaderTypeText(GLenum shaderType)
{
   const char * text;
   switch(shaderType)
   {
      case GL_COMPUTE_SHADER:         text = "Compute";                 break;
      case GL_VERTEX_SHADER:          text = "Vertex";                  break;
      case GL_TESS_CONTROL_SHADER:    text = "Tessallation Control";    break;
      case GL_TESS_EVALUATION_SHADER: text = "Tessellation Evaluation"; break;
      case GL_GEOMETRY_SHADER:        text = "Geometry";                break;
      case GL_FRAGMENT_SHADER:        text = "Fragment";                break;
      default:                        text = "Unknown";                 break;
   }
   return text;
}

GLuint reShader_loadCompileAndLink(const char * vertexShaderFilename, 
                                   const char * geometryShaderFilename,
                                   const char * fragmentShaderFilename)
{
   GLuint shaderProgramHandle;
   GLuint shaderHandles[3];
   GLenum shaderType[3];
   const char * shaderFilename[3];
   size_t shaderHandleCount = 0;
   size_t i;
   bool failedShaderBuild;
   GLenum errorCode;
   int linkFailureFlag;
   
   shaderProgramHandle = glCreateProgram();
   errorCode = glGetError();
   if(shaderProgramHandle == 0 || errorCode != GL_NO_ERROR)
   {
      rlError("glCreateProgram() returned %u. Reason: %s", shaderProgramHandle, gluErrorString(errorCode));
      return 0;
   }

   if(vertexShaderFilename != NULL)
   {
      shaderFilename[shaderHandleCount] = vertexShaderFilename;
      shaderType[shaderHandleCount]     = GL_VERTEX_SHADER;
      shaderHandleCount ++;
   }
   if(geometryShaderFilename != NULL)
   {
      shaderFilename[shaderHandleCount] = geometryShaderFilename;
      shaderType[shaderHandleCount]     = GL_GEOMETRY_SHADER;
      shaderHandleCount ++;
   }
   if(fragmentShaderFilename != NULL)
   {
      shaderFilename[shaderHandleCount] = fragmentShaderFilename;
      shaderType[shaderHandleCount]     = GL_FRAGMENT_SHADER;
      shaderHandleCount ++;
   }

   failedShaderBuild = false;
   for(i = 0; i < shaderHandleCount; i++)
   {
      shaderHandles[i] = reShader_loadAndCompile(shaderType[i], 
                                                 shaderFilename[i]);
      if(shaderHandles[i] != 0)
      {
         glAttachShader(shaderProgramHandle, shaderHandles[i]);
      }
      else
      {
         failedShaderBuild = true;
      }
   }

   if(failedShaderBuild)
   {
      for(i = 0; i < shaderHandleCount; i++)
      {
         if(shaderHandles[i] != 0)
         {
            glDeleteShader(shaderHandles[i]);
         }
      }
      glDeleteProgram(shaderProgramHandle);
      return 0;
   }

   linkFailureFlag = reShader_linkProgram(shaderProgramHandle);
   for(i = 0; i < shaderHandleCount; i++)
   {
      if(shaderHandles[i] != 0)
      {
         glDeleteShader(shaderHandles[i]);
      }
   }
   if(linkFailureFlag)
   {
      rlError("Linking of shader program failed using files: [%s, %s, %s]", vertexShaderFilename, geometryShaderFilename, fragmentShaderFilename);
      glDeleteProgram(shaderProgramHandle);
      return 0;
   }
   rlInfo("Successful Loaded Compiled and Linked program %d using files [%s, %s, %s]", (int)shaderProgramHandle, vertexShaderFilename, geometryShaderFilename, fragmentShaderFilename);
   return shaderProgramHandle;
}

#define FILECHUNKSIZE 128

static char * reShader_loadFile(const char * filename, size_t * filesize)
{
   FILE * fileHandle;
   char * fileData;
   size_t count;
   size_t allocatedSize;
   size_t readSize;

   fileHandle = fopen(filename, "r");
   if(fileHandle == NULL)
   {
      rlError("Could not open file for reading: %s\n", filename);
      return NULL;
   }
   allocatedSize = FILECHUNKSIZE;
   fileData = malloc(allocatedSize);
   count = 0;
   while((readSize = fread(&fileData[count], 1, FILECHUNKSIZE, fileHandle)) > 0)
   {
      count += readSize;
      if(count + FILECHUNKSIZE > allocatedSize)
      {
         allocatedSize += FILECHUNKSIZE;
         fileData = realloc(fileData, allocatedSize);
      }
   }
   fclose(fileHandle);
   if(filesize != NULL)
   {
      (*filesize) = count;
   }
   rlInfo("Loaded File %s of size %d bytes", filename, (int)count);
   return fileData;

}

GLuint reShader_loadAndCompile(GLenum shaderType, const char * shaderFilename)
{
   GLuint shaderHandle;
   char * fileContents;
   GLchar const * shaderTexts[1];
   GLint shaderLengths[1];
   size_t fileSize;
   GLenum errorCode;
   GLint compiledFlag;
   const char * shaderTypeText;

   shaderTypeText = reShader_getShaderTypeText(shaderType);

   fileContents = reShader_loadFile(shaderFilename, &fileSize);
   shaderTexts[0] = fileContents;
   shaderLengths[0] = (GLint)fileSize;
   if(shaderTexts[0] == NULL)
   {
      return 0;
   }

   shaderHandle = glCreateShader(shaderType);
   errorCode = glGetError();
   if(shaderHandle == 0 || errorCode != GL_NO_ERROR)
   {
      rlError("glCreateShader(%s) returned %u for filename %s. Reason: %s", shaderTypeText, shaderHandle, shaderFilename, gluErrorString(errorCode));
      free(fileContents);
      return 0;
   }
   
   glShaderSource(shaderHandle, 1, shaderTexts, shaderLengths);
   free(fileContents);
   if((errorCode = glGetError()) != GL_NO_ERROR)
   {
      rlError("glShaderSource failed for shader %u type %s on filename %s of size %d Reason: %s", shaderHandle, shaderTypeText, shaderFilename, (int)fileSize, gluErrorString(errorCode));
      glDeleteShader(shaderHandle);
      return 0;
   }
   
   glCompileShader(shaderHandle);
   // Check to see if the compilation failed
   glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compiledFlag);
   if(compiledFlag == GL_FALSE)
   {
      GLint logSize = 0;   
      glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &logSize);
      if(logSize > 0)
      {
         GLsizei slen = 0;
         GLchar* compilerLog = malloc(logSize);
         glGetShaderInfoLog(shaderHandle, logSize, &slen, compilerLog);
         rlError("Compile Log for shader %s of type %s:\n%s", shaderFilename, shaderTypeText, compilerLog);
         free(compilerLog);
      }
      else
      {
         rlError("Shader Compile Failed for shader %s of type %s but no log provided", shaderFilename, shaderTypeText);
      }
      glDeleteShader(shaderHandle);
      return 0;
   }
   rlInfo("Successfuly Loaded and Compiled Shader %d from file %s", (int)shaderHandle, shaderFilename);
   return shaderHandle;
}


int reShader_linkProgram(GLuint shaderProgramHandle)
{
   GLint linkedFlag;
   glLinkProgram(shaderProgramHandle);
   glGetProgramiv(shaderProgramHandle, GL_LINK_STATUS, &linkedFlag);
   if(linkedFlag == GL_FALSE)
   {
      GLint maxLength;
      GLsizei size;
      GLchar * linkLog;
      glGetProgramiv(shaderProgramHandle, GL_INFO_LOG_LENGTH, &maxLength);
      linkLog = malloc(maxLength);
      glGetProgramInfoLog(shaderProgramHandle, maxLength, &size, linkLog);
      rlError("Linking Shader Program %d Failed, Log:\n%s", (int)shaderProgramHandle, linkLog);
      free(linkLog);
      return 1;
   }
   rlInfo("Successfully linked Program %d", (int)shaderProgramHandle);
   return 0;
}

