#include <RyanColor.h>


const float rcAlphaOpaque = 1.0f;

const struct ryancolor rcColorWhite       = { 1.0f, 1.0f, 1.0f, rcAlphaOpaque };
const struct ryancolor rcColorBlack       = { 0.0f, 0.0f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorGrey        = { 0.5f, 0.5f, 0.5f, rcAlphaOpaque };
const struct ryancolor rcColorRed         = { 1.0f, 0.0f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorGreen       = { 0.0f, 1.0f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorBlue        = { 0.0f, 0.0f, 1.0f, rcAlphaOpaque };
const struct ryancolor rcColorYellow      = { 1.0f, 1.0f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorCyan        = { 0.0f, 1.0f, 1.0f, rcAlphaOpaque };
const struct ryancolor rcColorMagenta     = { 1.0f, 0.0f, 1.0f, rcAlphaOpaque };
const struct ryancolor rcColorDarkRed     = { 0.5f, 0.0f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorDarkGreen   = { 0.0f, 0.5f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorDarkBlue    = { 0.0f, 0.0f, 0.5f, rcAlphaOpaque };
const struct ryancolor rcColorDarkYellow  = { 0.5f, 0.5f, 0.0f, rcAlphaOpaque };
const struct ryancolor rcColorDarkCyan    = { 0.0f, 0.5f, 0.5f, rcAlphaOpaque };
const struct ryancolor rcColorDarkMagenta = { 0.5f, 0.0f, 0.5f, rcAlphaOpaque };

const struct ryancolor rcColorBlueBackground  = { 0.0f, 0.0f, 0.1f, rcAlphaOpaque };


