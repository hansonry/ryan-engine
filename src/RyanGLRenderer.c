#include <GL/glew.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <string.h>

#include <RyanGLRenderer.h>
#include <RyanRenderer.h>
#include <RyanColor.h>
#include <RyanIO.h>
#include <RyanLog.h>

static bool FirstInstance = true;

static const char * reShader_getShaderTypeText(GLenum shaderType)
{
   const char * text;
   switch(shaderType)
   {
      case GL_COMPUTE_SHADER:         text = "Compute";                 break;
      case GL_VERTEX_SHADER:          text = "Vertex";                  break;
      case GL_TESS_CONTROL_SHADER:    text = "Tessallation Control";    break;
      case GL_TESS_EVALUATION_SHADER: text = "Tessellation Evaluation"; break;
      case GL_GEOMETRY_SHADER:        text = "Geometry";                break;
      case GL_FRAGMENT_SHADER:        text = "Fragment";                break;
      default:                        text = "Unknown";                 break;
   }
   return text;
}

static GLuint reShader_loadAndCompile(GLenum shaderType, struct ryanbyteinputstream * shaderStream);
static int reShader_linkProgram(GLuint shaderProgramHandle);

static GLuint reShader_loadCompileAndLink(struct ryanbyteinputstream * vertexShaderStream, 
                                          struct ryanbyteinputstream * geometryShaderStream,
                                          struct ryanbyteinputstream * fragmentShaderStream)
{
   GLuint shaderProgramHandle;
   GLuint shaderHandles[3];
   GLenum shaderType[3];
   struct ryanbyteinputstream * shaderStream[3];
   size_t shaderHandleCount = 0;
   size_t i;
   bool failedShaderBuild;
   const char * vertexShaderSourceName   = "[NULL]";
   const char * geometryShaderSourceName = "[NULL]";
   const char * fragmentShaderSourceName = "[NULL]";
   GLenum errorCode;
   int linkFailureFlag;
   
   shaderProgramHandle = glCreateProgram();
   errorCode = glGetError();
   if(shaderProgramHandle == 0 || errorCode != GL_NO_ERROR)
   {
      rlError("glCreateProgram() returned %u. Reason: %s", shaderProgramHandle, gluErrorString(errorCode));
      return 0;
   }

   if(vertexShaderStream != NULL)
   {
      shaderStream[shaderHandleCount] = vertexShaderStream;
      vertexShaderSourceName          = vertexShaderStream->id;
      shaderType[shaderHandleCount]   = GL_VERTEX_SHADER;
      shaderHandleCount ++;
   }
   if(geometryShaderStream != NULL)
   {
      shaderStream[shaderHandleCount] = geometryShaderStream;
      geometryShaderSourceName        = geometryShaderStream->id;
      shaderType[shaderHandleCount]   = GL_GEOMETRY_SHADER;
      shaderHandleCount ++;
   }
   if(fragmentShaderStream != NULL)
   {
      shaderStream[shaderHandleCount] = fragmentShaderStream;
      fragmentShaderSourceName        = fragmentShaderStream->id;
      shaderType[shaderHandleCount]   = GL_FRAGMENT_SHADER;
      shaderHandleCount ++;
   }

   failedShaderBuild = false;
   for(i = 0; i < shaderHandleCount; i++)
   {
      shaderHandles[i] = reShader_loadAndCompile(shaderType[i], 
                                                 shaderStream[i]);
      if(shaderHandles[i] != 0)
      {
         glAttachShader(shaderProgramHandle, shaderHandles[i]);
      }
      else
      {
         failedShaderBuild = true;
      }
   }

   if(failedShaderBuild)
   {
      for(i = 0; i < shaderHandleCount; i++)
      {
         if(shaderHandles[i] != 0)
         {
            glDeleteShader(shaderHandles[i]);
         }
      }
      glDeleteProgram(shaderProgramHandle);
      return 0;
   }

   linkFailureFlag = reShader_linkProgram(shaderProgramHandle);
   for(i = 0; i < shaderHandleCount; i++)
   {
      if(shaderHandles[i] != 0)
      {
         glDeleteShader(shaderHandles[i]);
      }
   }
   if(linkFailureFlag)
   {
      rlError("Linking of shader program failed using files: [%s, %s, %s]", vertexShaderSourceName, geometryShaderSourceName, fragmentShaderSourceName);
      glDeleteProgram(shaderProgramHandle);
      return 0;
   }
   rlInfo("Successful Loaded Compiled and Linked program %d using files [%s, %s, %s]", (int)shaderProgramHandle, vertexShaderSourceName, geometryShaderSourceName, fragmentShaderSourceName);
   return shaderProgramHandle;
}

static GLuint reShader_loadAndCompile(GLenum shaderType, struct ryanbyteinputstream * shaderStream)
{
   GLuint shaderHandle;
   char * fileContents;
   GLchar const * shaderTexts[1];
   GLint shaderLengths[1];
   size_t fileSize;
   GLenum errorCode;
   GLint compiledFlag;
   const char * shaderTypeText;

   shaderTypeText = reShader_getShaderTypeText(shaderType);

   fileContents = rbisDumpToMemory(shaderStream, &fileSize, 0);
   shaderTexts[0] = fileContents;
   shaderLengths[0] = (GLint)fileSize;
   if(shaderTexts[0] == NULL)
   {
      return 0;
   }

   shaderHandle = glCreateShader(shaderType);
   errorCode = glGetError();
   if(shaderHandle == 0 || errorCode != GL_NO_ERROR)
   {
      rlError("glCreateShader(%s) returned %u for filename %s. Reason: %s", shaderTypeText, shaderHandle, shaderStream->id, gluErrorString(errorCode));
      free(fileContents);
      return 0;
   }

   glShaderSource(shaderHandle, 1, shaderTexts, shaderLengths);
   free(fileContents);
   if((errorCode = glGetError()) != GL_NO_ERROR)
   {
      rlError("glShaderSource failed for shader %u type %s on filename %s of size %d Reason: %s", shaderHandle, shaderTypeText, shaderStream->id, (int)fileSize, gluErrorString(errorCode));
      glDeleteShader(shaderHandle);
      return 0;
   }

   glCompileShader(shaderHandle);
   // Check to see if the compilation failed
   glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compiledFlag);
   if(compiledFlag == GL_FALSE)
   {
      GLint logSize = 0;   
      glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &logSize);
      if(logSize > 0)
      {
         GLsizei slen = 0;
         GLchar* compilerLog = malloc(logSize);
         glGetShaderInfoLog(shaderHandle, logSize, &slen, compilerLog);
         rlError("Compile Log for shader %s of type %s:\n%s", shaderStream->id, shaderTypeText, compilerLog);
         free(compilerLog);
      }
      else
      {
         rlError("Shader Compile Failed for shader %s of type %s but no log provided", shaderStream->id, shaderTypeText);
      }
      glDeleteShader(shaderHandle);
      return 0;
   }

   rlInfo("Successfuly Loaded and Compiled Shader %d from file %s", (int)shaderHandle, shaderStream->id);
   return shaderHandle;
}


static int reShader_linkProgram(GLuint shaderProgramHandle)
{
   GLint linkedFlag;
   glLinkProgram(shaderProgramHandle);
   glGetProgramiv(shaderProgramHandle, GL_LINK_STATUS, &linkedFlag);
   if(linkedFlag == GL_FALSE)
   {
      GLint maxLength;
      GLsizei size;
      GLchar * linkLog;
      glGetProgramiv(shaderProgramHandle, GL_INFO_LOG_LENGTH, &maxLength);
      linkLog = malloc(maxLength);
      glGetProgramInfoLog(shaderProgramHandle, maxLength, &size, linkLog);
      rlError("Linking Shader Program %d Failed, Log:\n%s", (int)shaderProgramHandle, linkLog);
      free(linkLog);
      return 1;
   }
   rlInfo("Successfully linked Program %d", (int)shaderProgramHandle);
   return 0;
}

struct rglrattribute
{
   struct rrattribute parent;
   struct rglrprogram * program;
   GLuint location;
   char * name;
};

struct rglrgeometry
{
   struct rrgeometry parent;
   struct ryanglrenderer * renderer;
   struct rglrvertexbuffer * vertexBuffer;
   struct rglrindexbuffer  * indexBuffer;
   GLenum drawMode;
   GLuint id;
   GLsizei strideInBytes;

   // Cacheing for Speed
   GLenum indexBufferType;
   size_t indexBufferElementSize;
   GLsizei indexBufferElementCount;
};

struct rglrvertexbuffer
{
   struct rrvertexbuffer parent;
   struct ryanglrenderer * renderer;
   enum rrbufferupdatehint updateHint;
   enum rrdatatype type;
   GLuint id;
};

struct rglrindexbuffer
{
   struct rrindexbuffer parent;
   struct ryanglrenderer * renderer;
   enum rrbufferupdatehint updateHint;
   enum rrdatatype type;
   size_t sizeInElements;
   GLuint id;
};

struct rglrprogram
{
   struct rrprogram parent;
   struct ryanglrenderer * renderer;
   GLuint id;
};

struct rglruniform
{
   struct rruniform parent;
   struct rglrprogram * program;
   GLint id;
   char * name;
};

struct ryanglrenderer
{
   struct ryanrenderer parent;
   struct rglrprogram * usedProgram;
};

static inline GLenum GetUsageFromUpdateHint(enum rrbufferupdatehint hint)
{
   GLenum usage;
   switch(hint)
   {
   default:
      rlError("hint is not an expected type: %d", (int)hint);
   case eRRBUH_Never:
      usage = GL_STATIC_DRAW;
      break;
   case eRRBUH_Sometimes:
      usage = GL_DYNAMIC_DRAW;
      break;
   case eRRBUH_Often:
      usage = GL_DYNAMIC_DRAW;
      break;
   }
   return usage;
}

static inline GLenum GetGLDatatypeFromDataType(enum rrdatatype type)
{
   GLenum glType;
   switch(type)
   {
   default:
      rlError("Unknown Type Specified: %d", (int)type);
   case eRRDT_UInt8:
      glType = GL_UNSIGNED_BYTE;
      break;
   case eRRDT_SInt8:
      glType = GL_BYTE;
      break;
   case eRRDT_UInt16:
      glType = GL_UNSIGNED_SHORT;
      break;
   case eRRDT_SInt16:
      glType = GL_SHORT;
      break;
   case eRRDT_UInt32:
      glType = GL_UNSIGNED_INT;
      break;
   case eRRDT_SInt32:
      glType = GL_INT;
      break;
   case eRRDT_Float32:
      glType = GL_FLOAT;
      break;
   }

   return glType;
}

static inline GLenum GetGLDrawModeFromDrawMode(enum rrdrawmode drawMode)
{
   GLenum glDrawMode;
   switch(drawMode)
   {
   default:
      rlError("Unknown Draw mode: %d", (int)drawMode);
   case eRRDM_Points:
      glDrawMode = GL_POINTS;
      break;
   case eRRDM_Lines:
      glDrawMode = GL_LINES;
      break;
   case eRRDM_Triangles:
      glDrawMode = GL_TRIANGLES;
      break;
   }

   return glDrawMode;
}

static inline size_t GetByteSizeFromDataType(enum rrdatatype type, size_t count)
{
   size_t baseSize;
   switch(type)
   {
   default:
      rlError("Unknown Type Specified: %d", (int)type);
      baseSize = 0;
      break;
   case eRRDT_UInt8:
   case eRRDT_SInt8:
      baseSize = 1;
      break;
   case eRRDT_UInt16:
   case eRRDT_SInt16:
      baseSize = 2;
      break;
   case eRRDT_UInt32:
   case eRRDT_SInt32:
   case eRRDT_Float32:
      baseSize = 4;
      break;
   }

   return baseSize * count;
}

#define TYPE_SET(type, name, src)  \
type * name = (type *)src

static void rglraDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglrattribute, rglra, ro); 
   free(rglra->name);
   free(rglra);
}

static struct rrprogram * rglraGetProgram(const struct rrattribute * rra)
{
   TYPE_SET(const struct rglrattribute, rglra, rra); 
   return &rglra->program->parent;
}

static const char * rglraGetName(const struct rrattribute * rra)
{
   TYPE_SET(const struct rglrattribute, rglra, rra); 
   return rglra->name;
}

static void rglrgDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglrgeometry, rglrg, ro); 
   glDeleteVertexArrays(1, &rglrg->id);
   free(rglrg);
}

static struct ryanrenderer * rglrgGetRenderer(const struct rrgeometry * rrg)
{
   TYPE_SET(const struct rglrgeometry, rglrg, rrg);
   return &rglrg->renderer->parent;
}

static struct rrvertexbuffer * rglrgGetVertexBuffer(const struct rrgeometry * rrg)
{
   TYPE_SET(const struct rglrgeometry, rglrg, rrg);
   return &rglrg->vertexBuffer->parent;
}

static struct rrindexbuffer * rglrgGetIndexBuffer(const struct rrgeometry * rrg)
{
   TYPE_SET(const struct rglrgeometry, rglrg, rrg);
   return &rglrg->indexBuffer->parent;
}
static void rglrgAddBufferData(struct rrgeometry * rrg, 
                               struct rrattribute * rra,
                               size_t sizeInElements,
                               size_t firstOffsetInElements)
{
   TYPE_SET(struct rglrgeometry, rglrg, rrg);
   TYPE_SET(struct rglrattribute, rglra, rra);
   
   if(rglrg->renderer == rglra->program->renderer)
   {
      GLenum type = GetGLDatatypeFromDataType(rglrg->vertexBuffer->type);
      size_t elementSize = GetByteSizeFromDataType(rglrg->vertexBuffer->type, 1);

      glBindVertexArray(rglrg->id);
      glEnableVertexAttribArray(rglra->location);

      glVertexAttribPointer(rglra->location, sizeInElements, type, GL_FALSE, 
                            rglrg->strideInBytes,
                            (const void*)(firstOffsetInElements * elementSize));

      glBindVertexArray(0);
   }
   else
   {
      rlError("The Renderer of Geometry %p does not match the renderer of Attribute %p", rglrg, rra);
   }
}

static void rglrgDrawPart(struct rrgeometry * rrg, size_t startIndex, size_t indexCount)
{
   TYPE_SET(struct rglrgeometry, rglrg, rrg);

   glBindVertexArray(rglrg->id);
   glDrawElements(rglrg->drawMode, indexCount, rglrg->indexBufferType, 
                  (const void *)(startIndex * rglrg->indexBufferElementSize));

   // Not unbinding for speed. May cause problems
}

static void rglrgDraw(struct rrgeometry * rrg)
{
   TYPE_SET(struct rglrgeometry, rglrg, rrg);

   glBindVertexArray(rglrg->id);
   glDrawElements(rglrg->drawMode, rglrg->indexBufferElementCount, 
                  rglrg->indexBufferType, (const void *)0);

   // Not unbinding for speed. May cause problems
}

static void rglrvbDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglrvertexbuffer, rglrvb, ro); 
   glDeleteBuffers(1, &rglrvb->id);
   free(rglrvb);
}

static struct ryanrenderer * rglrvbGetRenderer(const struct rrvertexbuffer * rrvb)
{
   TYPE_SET(const struct rglrvertexbuffer, rglrvb, rrvb);
   return &rglrvb->renderer->parent;
}

static void rglribDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglrindexbuffer, rglrib, ro); 
   glDeleteBuffers(1, &rglrib->id);
   free(rglrib);
}

static struct ryanrenderer * rglribGetRenderer(const struct rrindexbuffer * rrib)
{
   TYPE_SET(const struct rglrindexbuffer, rglrib, rrib);
   return &rglrib->renderer->parent;
}


static void rglruDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglruniform, rglru, ro); 
   free(rglru->name);
   free(rglru);
}


static struct rrprogram * rglruGetProgram(const struct rruniform * rru)
{
   TYPE_SET(const struct rglruniform, rglru, rru); 
   return &rglru->program->parent;
}

static const char * rglruGetName(const struct rruniform * rru)
{
   TYPE_SET(const struct rglruniform, rglru, rru); 
   return rglru->name;
}

static void rglruSetMatrix4f(struct rruniform * rru, float * matrixData, size_t matrixCount)
{
   TYPE_SET(struct rglruniform, rglru, rru); 
   rrpUse(&rglru->program->parent);
   glUniformMatrix4fv(rglru->id, matrixCount, GL_FALSE, matrixData);
}

static void rglrpDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct rglrprogram, rglrp, ro);
   glDeleteProgram(rglrp->id);
   free(rglrp);
}

static struct ryanrenderer * rglrpGetRenderer(const struct rrprogram * rrp)
{
   TYPE_SET(const struct rglrprogram, rglrp, rrp);
   return &rglrp->renderer->parent;
}

static void rglrpUse(struct rrprogram * rrp)
{
   TYPE_SET(struct rglrprogram, rglrp, rrp);
   if(rglrp->renderer->usedProgram != rglrp)
   {
      rglrp->renderer->usedProgram = rglrp;
      glUseProgram(rglrp->id);
   }

}

static enum ryanrenderertype rglrGetRendererType(const struct ryanrenderer * rr)
{
   (void)rr;
   return eRRT_OpenGL;
}

static void rglrDestroy(struct ryanobject * ro)
{
   TYPE_SET(struct ryanglrenderer, rglr, ro); 
   free(rglr);
}

static void rglrSetClearColor(struct ryanrenderer * rr, 
                              const struct ryancolor * color)
{
   glClearColor(color->r, color->g, color->b, color->a);
}

static void rglrClearColor(struct ryanrenderer * rr)
{
   glClear(GL_COLOR_BUFFER_BIT);
}

static void rglrClearDepth(struct ryanrenderer * rr)
{
   glClear(GL_DEPTH_BUFFER_BIT);
}

static void rglrClearColorAndDepth(struct ryanrenderer * rr)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

#define SET_FUNCTION(so, pref, name) so->fp##name = pref##name
#define SET_DESTROY_FUNCTION(so, pref) so->parent.fpDestroy = pref##Destroy

static struct rruniform * rglrpCreateUniformFromName(struct rrprogram * rrp, 
                                                     const char * name)
{
   TYPE_SET(struct rglrprogram, rglrp, rrp); 
   struct rglruniform * rglru;
   struct rruniform * rru;
   GLint id;

   id = glGetUniformLocation(rglrp->id, name);
   if(id < 0)
   {
      rlError("Failed to find Uniform named: %s", name);
      return NULL;
   }

   rglru = malloc(sizeof(struct rglruniform));
   rru = &rglru->parent;

   rglru->name = strdup(name);
   rglru->program = rglrp;
   rglru->id = id;

   SET_DESTROY_FUNCTION(rru, rglru);
   SET_FUNCTION(rru, rglru, GetProgram);
   SET_FUNCTION(rru, rglru, SetMatrix4f);

   return rru;

}

static inline struct rglrattribute * CreateGLAttribute(struct rglrprogram * program, GLuint location)
{
   struct rglrattribute * rglra = malloc(sizeof(struct rglrattribute));
   struct rrattribute * rra = &rglra->parent;

   rglra->program = program;
   rglra->location = location;

   SET_DESTROY_FUNCTION(rra, rglra);
   SET_FUNCTION(rra, rglra, GetProgram);
   SET_FUNCTION(rra, rglra, GetName);

   return rglra;
}

static struct rrattribute * rglrpCreateAttributeFromName(struct rrprogram * rrp, const char * name)
{
   TYPE_SET(struct rglrprogram, rglrp, rrp); 
   struct rglrattribute * rglra;
   GLint result;

   result = glGetAttribLocation(rglrp->id, name);

   if(result < 0)
   {
      rlError("Failed to find attribute \"%s\" in program %p", name, rglrp);
      return NULL;
   }

   rglra = CreateGLAttribute(rglrp, (GLuint)result);

   rglra->name = strdup(name);

   return &rglra->parent;
}

static inline bool FindGLAttributeByLocation(GLuint program, GLuint location, GLuint * index, char ** name)
{
   GLuint foundIndex;
   bool found = false;
   GLint count, i;
   char * lName = NULL;
   GLint nameSize;
   GLint size;
   GLsizei length;
   GLenum type;
   (void)type; // type is never read, only set
   (void)length; // length is never read only set
   (void)size; // size is never read and only set

   glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &count);
   glGetProgramiv(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &nameSize);

   lName = malloc(nameSize + 1);

   for(i = 0; i < count; i++)
   {
      GLint locationFind;
      // Create Name String from index.
      // If there is a better way to do this, I would like to know.
      glGetActiveAttrib(program, i, nameSize + 1, &length, 
                        &size, &type, lName);

      locationFind = glGetAttribLocation(program, lName);
      if(locationFind >= 0 && locationFind == locationFind)
      {
         found = true;
         foundIndex = i;
         break;
      }
   }

   if(found)
   {
      if(index != NULL)
      {
         *index = foundIndex;
      }
      if(name != NULL)
      {
         *name = lName;
      }
      else
      {
         free(lName);
      }
   }
   else
   {
      free(lName);
   }

   return found;
}

static struct rrattribute * rglrpCreateAttributeFromIndex(struct rrprogram * rrp, unsigned int index)
{
   TYPE_SET(struct rglrprogram, rglrp, rrp); 
   struct rglrattribute * rglra;
   char * name;

   if(!FindGLAttributeByLocation(rglrp->id, (GLuint)index, NULL, &name))
   {
      rlError("Failed to find GL Attribute Location: %d using program %p", (int)index, rglrp);
      return NULL;
   }

   rglra = CreateGLAttribute(rglrp, (GLuint)index);

   rglra->name = name;

   return &rglra->parent;
}


static struct rrprogram * rglrCreateProgramFromStreams(struct ryanrenderer * rr,
                                                       struct ryanbyteinputstream * vertexStream,
                                                       struct ryanbyteinputstream * geometryStream,
                                                       struct ryanbyteinputstream * fragmentStream)
{
   TYPE_SET(struct ryanglrenderer, rglr, rr); 
   struct rglrprogram * rglrp = malloc(sizeof(struct rglrprogram));
   struct rrprogram * rrp = &rglrp->parent;

   rglrp->renderer = rglr;
   rglrp->id = reShader_loadCompileAndLink(vertexStream, geometryStream, fragmentStream);

   // Restore previously bound program
   if(rglr->usedProgram == NULL)
   {
      glUseProgram(0);
   }
   else
   {
      glUseProgram(rglr->usedProgram->id);
   }

   SET_DESTROY_FUNCTION(rrp, rglrp);
   SET_FUNCTION(rrp, rglrp, Use);
   SET_FUNCTION(rrp, rglrp, CreateUniformFromName);
   SET_FUNCTION(rrp, rglrp, CreateAttributeFromName);
   SET_FUNCTION(rrp, rglrp, CreateAttributeFromIndex);

   return rrp;
}


static inline GLint CreateGLBuffer(GLenum target,
                                   enum rrdatatype type, 
                                   const void * data, 
                                   size_t sizeInElements, 
                                   enum rrbufferupdatehint updateHint)
{
   GLint id;
   GLenum usage = GetUsageFromUpdateHint(updateHint);


   glGenBuffers(1, &id);
   glBindBuffer(target, id);
   
   glBufferData(target, 
                GetByteSizeFromDataType(type, sizeInElements), 
                data, 
                usage);
   return id;
}

struct rrvertexbuffer * rglrCreateVertexBuffer(struct ryanrenderer * rr, 
                                               enum rrdatatype type, 
                                               const void * data,
                                               size_t sizeInElements,
                                               enum rrbufferupdatehint updateHint)
{
   TYPE_SET(struct ryanglrenderer, rglr, rr); 
   struct rglrvertexbuffer * rglrvb;
   struct rrvertexbuffer * rrvb;
   GLint id = CreateGLBuffer(GL_ARRAY_BUFFER, type, data, sizeInElements, 
                             updateHint);

   rglrvb = malloc(sizeof(struct rglrvertexbuffer));
   rrvb = &rglrvb->parent;

   rglrvb->id         = id;
   rglrvb->type       = type;
   rglrvb->updateHint = updateHint;
   rglrvb->renderer   = rglr;
   
   SET_DESTROY_FUNCTION(rrvb, rglrvb);
   SET_FUNCTION(rrvb, rglrvb, GetRenderer);

   return rrvb;
}

struct rrindexbuffer * rglrCreateIndexBuffer(struct ryanrenderer * rr, 
                                             enum rrdatatype type, 
                                             const void * data,
                                             size_t sizeInElements,
                                             enum rrbufferupdatehint updateHint)
{
   TYPE_SET(struct ryanglrenderer, rglr, rr); 
   struct rglrindexbuffer * rglrib;
   struct rrindexbuffer * rrib;

   if(type != eRRDT_UInt8  &&
      type != eRRDT_UInt16 &&
      type != eRRDT_UInt32)
   {
      rlError("Only unisgned integer types are support for indexes");
      return NULL;
   }

   GLint id = CreateGLBuffer(GL_ELEMENT_ARRAY_BUFFER, type, data, 
                             sizeInElements, updateHint);

   rglrib = malloc(sizeof(struct rglrindexbuffer));
   rrib = &rglrib->parent;

   rglrib->id             = id;
   rglrib->type           = type;
   rglrib->updateHint     = updateHint;
   rglrib->renderer       = rglr;
   rglrib->sizeInElements = sizeInElements;
   
   SET_DESTROY_FUNCTION(rrib, rglrib);
   SET_FUNCTION(rrib, rglrib, GetRenderer);

   return rrib;
}

struct rrgeometry * rglrCreateGeometry(struct ryanrenderer * rr,
                                       struct rrvertexbuffer * vertexBuffer,
                                       size_t vertexBufferStrideInElements,
                                       struct rrindexbuffer * indexBuffer,
                                       enum rrdrawmode drawMode)
{
   TYPE_SET(struct ryanglrenderer, rglr, rr); 
   TYPE_SET(struct rglrvertexbuffer, rglrvb, vertexBuffer); 
   TYPE_SET(struct rglrindexbuffer, rglrib, indexBuffer); 
   struct rglrgeometry * rglrg;
   struct rrgeometry * rrg;

   GLuint id;

   if(rrvbGetRenderer(vertexBuffer) != rr)
   {
      rlError("Renderer %p and Vertex Buffer Renderer  %p do not match", rr, rrvbGetRenderer(vertexBuffer));
      return NULL;
   }

   if(rribGetRenderer(indexBuffer) != rr)
   {
      rlError("Renderer %p and Index Buffer Renderer  %p do not match", rr, rribGetRenderer(indexBuffer));
      return NULL;
   }

   glGenVertexArrays(1, &id);
   glBindVertexArray(id);

   glBindBuffer(GL_ARRAY_BUFFER, rglrvb->id);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rglrib->id);
   
   glBindVertexArray(0);

   rglrg = malloc(sizeof(struct rglrgeometry));
   rrg = &rglrg->parent;

   rglrg->renderer      = rglr;
   rglrg->id            = id;
   rglrg->drawMode      = GetGLDrawModeFromDrawMode(drawMode);
   rglrg->strideInBytes = GetByteSizeFromDataType(rglrvb->type, 
                                                  vertexBufferStrideInElements);
   rglrg->vertexBuffer  = rglrvb;
   rglrg->indexBuffer   = rglrib;

   // Cacheing for speed
   rglrg->indexBufferType         = GetGLDatatypeFromDataType(rglrib->type);
   rglrg->indexBufferElementSize  = GetByteSizeFromDataType(rglrib->type, 1);
   rglrg->indexBufferElementCount = (GLsizei)rglrib->sizeInElements;
   
   SET_DESTROY_FUNCTION(rrg, rglrg);
   SET_FUNCTION(rrg, rglrg, GetRenderer);
   SET_FUNCTION(rrg, rglrg, GetVertexBuffer);
   SET_FUNCTION(rrg, rglrg, GetIndexBuffer);
   SET_FUNCTION(rrg, rglrg, AddBufferData);
   SET_FUNCTION(rrg, rglrg, Draw);
   SET_FUNCTION(rrg, rglrg, DrawPart);

   return rrg;
}

static inline void rglrLogGLInfo(void)
{
   rlInfo("OpenGL Vendor: %s", glGetString(GL_VENDOR));
   rlInfo("OpenGL Renderer: %s", glGetString(GL_RENDERER));
   rlInfo("OpenGL Version: %s", glGetString(GL_VERSION));
   rlInfo("OpenGL Shader Language Version: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

struct ryanrenderer * rrCreateGLRenderer(void)
{
   struct ryanglrenderer * rglr = malloc(sizeof(struct ryanglrenderer));
   struct ryanrenderer * rr = &rglr->parent;

   if(FirstInstance)
   {
      FirstInstance = false;
      rglrLogGLInfo();
   }

   SET_DESTROY_FUNCTION(rr, rglr);
   SET_FUNCTION(rr, rglr, GetRendererType);
   SET_FUNCTION(rr, rglr, SetClearColor);
   SET_FUNCTION(rr, rglr, ClearColor);
   SET_FUNCTION(rr, rglr, ClearDepth);
   SET_FUNCTION(rr, rglr, ClearColorAndDepth);
   SET_FUNCTION(rr, rglr, CreateProgramFromStreams);
   SET_FUNCTION(rr, rglr, CreateVertexBuffer);
   SET_FUNCTION(rr, rglr, CreateIndexBuffer);
   SET_FUNCTION(rr, rglr, CreateGeometry);

   glEnableClientState(GL_VERTEX_ARRAY);
   glEnableClientState(GL_INDEX_ARRAY);
   
   rglr->usedProgram = NULL;
   glUseProgram(0);
   return rr;
}



