#include <stdio.h>
#include <RyanLog.h>

#ifndef RL_DEFAULT_FILENAME
#define RL_DEFAULT_FILENAME "RyanLog.txt"
#endif // RL_DEFAULT_FILENAME

static int MaxFileLevel    = RL_LEVEL_DEBUG;
static int MaxConsoleLevel = RL_LEVEL_ERROR;
static FILE * LogFileHandle = NULL;

void rlInit(void)
{
   const char * logFilename = RL_DEFAULT_FILENAME;
   LogFileHandle = fopen(logFilename, "w");
   if(LogFileHandle == NULL)
   {
      rlError("Failed To Open Log File for writing: %s\n", logFilename);
   }
}

void rlCleanup(void)
{
   if(LogFileHandle != NULL)
   {
      fclose(LogFileHandle);
      LogFileHandle = NULL;
   }
}

int rlSetMaxLevel(int level)
{
   rlSetMaxFileLevel(level);
   return rlSetMaxConsoleLevel(level);
}
int rlSetMaxFileLevel(int level)
{
   int oldLevel = MaxFileLevel;
   if(level > RL_LEVEL_TRACE)
   {
      MaxFileLevel = RL_LEVEL_TRACE;
   }
   else
   {
      MaxFileLevel = level;
   }
   rlInfo("Max File Level set to: %d", MaxFileLevel);
   return oldLevel;
}
int rlSetMaxConsoleLevel(int level)
{
   int oldLevel = MaxConsoleLevel;
   if(level > RL_LEVEL_TRACE)
   {
      MaxConsoleLevel = RL_LEVEL_TRACE;
   }
   else
   {
      MaxConsoleLevel = level;
   }
   rlInfo("Max Console Level set to: %d", MaxConsoleLevel);
   return oldLevel;
}

static const char * rlGetLevelText(int level)
{
   const char * text;
   switch(level)
   {
      case RL_LEVEL_CRITICAL: text = "CRITICAL";      break;
      case RL_LEVEL_ERROR:    text = "ERROR";         break;
      case RL_LEVEL_WARNING:  text = "warning";       break;
      case RL_LEVEL_INFO:     text = "info";          break;
      case RL_LEVEL_DEBUG:    text = "debug";         break;
      case RL_LEVEL_TRACE:    text = "trace";         break;
      default:                text = "UNKNOWN LEVEL"; break;
   }
   return text;
}

static const char * rlFindStartOfFileName(const char * filename)
{
   const char * start, * seek;

   start = filename;
   seek = filename;
   while(*seek != '\0')
   {
      if(*seek == '/' || *seek == '\\')
      {
         start = seek + 1;
      }
      seek ++;
   }

   return start;
}

void rlLogV(const char * filename, int line, int level, const char * msgfmt, va_list args)
{
   va_list file_args;
   const char * levelText;
   const char * filenameNoPath = rlFindStartOfFileName(filename);
   levelText = rlGetLevelText(level);
   if(level <= MaxFileLevel && LogFileHandle != NULL)
   {
      va_copy(file_args, args);
      fprintf(LogFileHandle, "[%s] %s:%d: ", levelText, filenameNoPath, line);
      vfprintf(LogFileHandle, msgfmt, file_args);
      fprintf(LogFileHandle, "\n");
      fflush(LogFileHandle);
      va_end(file_args);
   }

   if(level <= MaxConsoleLevel)
   {
      fprintf(stderr, "[%s] %s:%d: ", levelText, filenameNoPath, line);
      vfprintf(stderr, msgfmt, args);
      fprintf(stderr, "\n");
   }
}

void rlLog(const char * filename, int line, int level, const char * msgfmt, ...)
{
   va_list args;
   va_start(args, msgfmt);
   rlLogV(filename, line, level, msgfmt, args);
   va_end(args);
}

