# Build Instructions

## Linux Build

### Setup
Install the following tools/libraries:

* cmake
* SDL2
* OpenGL
* GLEW


Run the following commands to configure the build:
1. `cmake .` (you only need to do this once)

### Build

Run the following commands to build.

1. `make`

After that you should have built the library and all the examples.

## Windows Build using MSYS2

### Setup
First you need to grab MSYS2. Last time I checked that lived at 
https://www.msys2.org/

Next you need run the MSYS2 of your choice, you will need to install
a bunch of stuff

1. `pacman -Syu`
2. `pacman -Su`
3. `pacman -S mingw-w64-x86_64-gcc` (This is for 64 bit builds, you can try 
   `mingw-w64-i686-gcc` for 32 bit builds)
4. `pacman -S mingw-w64-SDL2 mingw-w64-x86_64-glew mingw-w64-x86_64-headers-git cmake git`

Next you will need to run the first pass of cmake. This will fail because we
need to manulay specify the paths for the libraries.

1. `cmake .`

After that fails there should be a file `CMakeCache.txt`. Open that up in 
a text editor and ajust the following parameters. I have put in sugjested
values based on how my MSYS2 is configured:

* OPENGL_EGL_INCLUDE_DIR:PATH=/mingw64/include
* OPENGL_GLX_INCLUDE_DIR:PATH=/mingw64/include
* OPENGL_INCLUDE_DIR:PATH=/mingw64/include
* OPENGL_glu_LIBRARY:FILEPATH=glu32
* OPENGL_glx_LIBRARY:FILEPATH=glx32
* OPENGL_opengl_LIBRARY:FILEPATH=opengl32
* SDL2_INCLUDE_DIRS:FILEPATH=/mingw64/include/SDL2
* SDL2_LIBRARIES:FILEPATH=mingw32;SDL2main;SDL2
* GLEW_SHARED_LIBRARY_RELEASE:STRING=glew32